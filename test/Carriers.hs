{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE OverloadedStrings #-}


module Carriers where

import Import
import Ephemeris.Types
import Effects (TimeZoneData (TimeAtPointToUTC), EphemerisData (..))

data FixedEphemeris = FixedEphemeris
  {
    fixedPlanetPositions :: [(Planet, EclipticPosition)]
  , fixedHouseCusps :: CuspsCalculation
  , fixedObliquity :: ObliquityInformation
  }

newtype FixedTimeZone = 
  FixedTimeZone Text
  deriving newtype (Eq, Show, IsString)

---
--- ALGEBRAE
--- 

instance (Algebra sig m)
  => Algebra (TimeZoneData :+: sig) (TimeZoneTestCarrier m) where
  alg hdl sig ctx = TimeZoneTestCarrier $ case sig of
    L (TimeAtPointToUTC _lt _lg localTime) -> do
      tz <- ask
      let timezone = mkTimeZone tz
      return $ (<$ ctx) $ localTimeToUTC timezone localTime
    R other -> alg (runTimeZoneTest . hdl) (R other) ctx

instance (Algebra sig m)
  => Algebra (EphemerisData :+: sig) (EphemerisTestCarrier m) where
  alg hdl sig ctx = EphemerisTestCarrier $ case sig of
    L (GetPlanetPosition p _time) -> do
      fixedData <- ask 
      let planets = fixedData & fixedPlanetPositions
      return $ (<$ ctx) $ maybeToRight "Planet not found" (lookup p planets)
    L (GetHouseCusps _hsystem _time _geopos) -> do
      fixedData <- ask
      return $ (<$ ctx) $ fixedData & fixedHouseCusps
    L (GetObliquity _time) -> do
      fixedData <- ask
      return $ (<$ ctx) $ fixedData & fixedObliquity & Right
    R other -> alg (runEphemerisTest . hdl) (R other) ctx
---
--- CARRIERS
---

newtype EphemerisTestCarrier m a =
  EphemerisTestCarrier { runEphemerisTest :: ReaderC FixedEphemeris m a}
  deriving (Applicative, Functor, Monad, MonadIO, MonadFail)
  
runEphemerisTestWith :: FixedEphemeris -> EphemerisTestCarrier m a -> m a
runEphemerisTestWith fixedEphe = runReader fixedEphe . runEphemerisTest

newtype TimeZoneTestCarrier m a =
  TimeZoneTestCarrier { runTimeZoneTest :: ReaderC FixedTimeZone m a }
  deriving (Applicative, Functor, Monad, MonadIO, MonadFail)
  
runTimeZoneTestWith :: FixedTimeZone -> TimeZoneTestCarrier m a -> m a
runTimeZoneTestWith fixedTZ = runReader fixedTZ . runTimeZoneTest


---
--- HELPERS
---

mkTimeZone :: FixedTimeZone -> TimeZone
mkTimeZone "EST" = TimeZone (-300) False "EST"
mkTimeZone "EDT" = TimeZone (-240) False "EDT"
mkTimeZone _ = utc
