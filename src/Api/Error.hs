{-# LANGUAGE DeriveGeneric #-}
module Api.Error (ServerError, serverError, errorResponse, renderErrorResponse) where

import Import
import Data.Aeson
import qualified Data.ByteString.Lazy as LB

-- cf. https://spec.graphql.org/June2018/#sec-Errors
-- https://hackage.haskell.org/package/morpheus-graphql-core-0.17.0/docs/src/Data.Morpheus.Types.Internal.AST.Base.html#GQLError

data ErrorResponse = 
  ErrorResponse
    {
      _errors :: [ServerError]
    , _data :: Maybe Value
    } deriving (Show, Eq, Generic)
    
instance ToJSON ErrorResponse where
  toJSON = genericToJSON defaultOptions {fieldLabelModifier = drop 1}

data ServerError =
  ServerError
    {
      _message :: Text
    , _locations :: [Value]
    , _path :: [Value]
    , _extensions :: Maybe Value
    } deriving (Show, Eq, Generic)

instance ToJSON ServerError where
  toJSON = genericToJSON defaultOptions {fieldLabelModifier = drop 1}
    
    
-- | Simple server error: only message, no locations, path or extensions.
serverError :: Text -> ServerError
serverError msg = ServerError msg [] [] Nothing
  
-- | Collect a single error into an error response; no partial data included.
errorResponse :: ServerError -> ErrorResponse
errorResponse err = ErrorResponse [err] Nothing 

-- | Given a single error, render it as a full error response, encoded as 
-- a JSON bytestring
renderErrorResponse :: ServerError -> LB.ByteString
renderErrorResponse = encode . errorResponse
