module Main where

import qualified Api.Main as EphemerisServer

main :: IO ()
main = EphemerisServer.main
