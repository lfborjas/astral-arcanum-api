{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}

module Config where

import Import
import System.Envy
  ( FromEnv (..),
    Option (dropPrefixCount),
    Var (..),
    defOption,
    gFromEnvCustom,
  )
import Data.Time.LocalTime.TimeZone.Detect (TimeZoneDatabase)
import qualified Data.Pool as P
import qualified Database.PostgreSQL.Simple as PG

data Environment
  = Development
  | Test
  | Production
  deriving stock (Eq, Show, Enum, Read)

instance Var Environment where
  toVar = show
  fromVar = readMaybe

newtype DatabaseUrl = DatabaseUrl Text
  deriving newtype (Eq, Show)
  deriving (Var) via Text

-- | Configuration as it comes from the environment; flat, static.
data AppConfig = AppConfig
  { appPort :: !Int,
    appDeployEnv :: !Environment,
    appTimeZoneFile :: !FilePath,
    appEphemerisFolder :: !FilePath,
    appDatabaseUrl :: !DatabaseUrl,
    appJwtSecret :: !Text,
    appPostmarkApiToken :: !Text
  }
  deriving stock (Eq, Show, Generic)

instance FromEnv AppConfig where
  -- drop the `app*` prefix 
  fromEnv = gFromEnvCustom defOption {dropPrefixCount = 3}

data AppSecrets = 
  AppSecrets
    { jwtSecret :: Text
    , postmarkApiToken :: Text
    }
-- opaque "env" to carry/specify runtime dependencies.
data AppContext = AppContext
    {
       timeZoneDatabase :: !TimeZoneDatabase,
       databasePool :: P.Pool PG.Connection,
       secrets :: AppSecrets
    }

-- | Default app config. Override with environment variables.
defaultConfig :: AppConfig
defaultConfig =
  AppConfig
    { appPort = 3000,
      appDeployEnv = Development,
      appEphemerisFolder = "./config",
      appTimeZoneFile = "./config/timezone21.bin",
      appDatabaseUrl = DatabaseUrl "postgresql://localhost/astral_arcanum_dev?user=luis",
      appJwtSecret = "tacet",
      appPostmarkApiToken = "POSTMARK_API_TEST"
      -- the underlying lib can parse the right stuff here:
      -- https://hackage.haskell.org/package/hedis-0.14.1/docs/Database-Redis.html#v:parseConnectInfo
      --appRedisUrl = RedisUrl "redis://"
    }

-- | Log levels
data LogMessage
  = Debug Text
  | Info Text
  | Warn Text
  | Error Text

renderLogMessage :: LogMessage -> Text
renderLogMessage = \case
  Debug m -> "[DEBUG] " <> m
  Info m -> "[INFO] " <> m
  Warn m -> "[WARN] " <> m
  Error m -> "[ERROR] " <> m
