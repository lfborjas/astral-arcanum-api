{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}
module Api.DomainUtils 
  ( horoscope,
    moment,
    Location (..),
    BirthData (..),
  )
where

import Data.Time.LocalTime ( zonedTimeToUTC )
import Effects
    ( EphemerisData,
      getPlanetPosition,
      getHouseCusps,
      getObliquity, Log, log, toJulianUT1, fromJulianUT1)
import Ephemeris.Aspect (celestialAspects, planetaryAspects)
import Ephemeris.Planet (defaultPlanets)
import Ephemeris.Types
    ( CuspsCalculation(CuspsCalculation),
      EclipticPosition(lat, lngSpeed, lng),
      EquatorialPosition(declination),
      GeographicPosition(..),
      HouseCusp,
      HouseSystem(Placidus),
      JulianDayUT1,
      ObliquityInformation(..),
      HoroscopeData(HoroscopeData),
      House(House),
      HouseNumber(XII, I),
      Latitude(..),
      Longitude(..),
      PlanetPosition(..), MomentData (..) )
import Ephemeris.Utils (mkEcliptic)
import Import
    ( ($),
      Eq,
      Monad(return),
      Show,
      Applicative(pure),
      Semigroup((<>)),
      Maybe(Just, Nothing),
      Either(Right, Left),
      Text,
      zipWith,
      (.),
      forM,
      (&),
      catMaybes,
      curry,
      show, Generic, error,ToText (toText), UTCTime, ZonedTime )
import SwissEphemeris (eclipticToEquatorial)
import Ephemeris.House (housePosition)
import Config
import Control.Carrier.Error.Either

data Location = Location
  { locationInput :: Maybe Text,
    locationLatitude :: Latitude,
    locationLongitude :: Longitude
  }
  deriving (Eq, Show, Generic)

data BirthData = BirthData
  { birthLocation :: Location,
    birthTime :: ZonedTime
  }
  deriving (Show, Generic)

horoscope :: (Has EphemerisData sig m, Has (Log LogMessage) sig m) 
  => BirthData -> m HoroscopeData
horoscope BirthData {..} = do
  let uTime = zonedTimeToUTC birthTime
      place = locationToGeo birthLocation
  time' <- toJulianUT1 uTime

  case time' of
    Left e-> error .toText $ e
    Right time -> do
      obliquity <- obliquityOrBust time
      (CuspsCalculation cusps angles' sys) <- getHouseCusps Placidus time place
      let houses' = houses obliquity cusps
      positions <- planetPositions houses' obliquity time
      return $
        HoroscopeData
          positions
          angles'
          (houses obliquity cusps)
          sys
          (planetaryAspects positions)
          (celestialAspects positions angles')
          uTime
          time

-- | Planet positions, and their aspects, for a given moment in time. 
moment :: (Has EphemerisData sig m, Has (Log LogMessage) sig m) 
  => UTCTime -> m MomentData
moment uTime = do
  time' <- toJulianUT1 uTime
  case time' of
    Left e-> error . toText $ e
    Right time -> do
      obliquity <- obliquityOrBust time
      positions <- planetPositions [] obliquity time
      utTime <- fromJulianUT1 time
      return $
        MomentData
          { momentPlanetPositions = positions,
            momentPlanetaryAspects = planetaryAspects positions,
            momentUniversalTime = utTime,
            momentJulianTime = time
          }

locationToGeo :: Location -> GeographicPosition
locationToGeo Location {..} =
  GeographicPosition
    { geoLat = locationLatitude & unLatitude,
      geoLng = locationLongitude & unLongitude
    }

obliquityOrBust :: (Has EphemerisData sig m) => JulianDayUT1 -> m ObliquityInformation
obliquityOrBust time = do
  obliquity <- getObliquity time
  case obliquity of
    Left e -> error . toText $ "Unable to calculate obliquity: " <> e <> " (for time: " <> show time <> ")"
    Right o -> pure o

planetPositions :: (Has EphemerisData sig m, Has (Log LogMessage) sig m) => [House] -> ObliquityInformation -> JulianDayUT1 -> m [PlanetPosition]
planetPositions houses' o@ObliquityInformation {} time = do
  maybePositions <- forM defaultPlanets $ \p -> do
    coords <- getPlanetPosition p time
    case coords of
      Left e -> do
        log $ Error $ "Error getting planet position: " <> toText e
        pure Nothing
      Right c -> do
        let decl = eclipticToEquatorial o c & declination
        pure $ Just $ 
          PlanetPosition
            { planetName = p 
            , planetLat = Latitude . lat $ c
            , planetLng = Longitude . lng $ c
            , planetLngSpeed = lngSpeed c
            , planetDeclination = decl
            , planetHouse = findHouse (Longitude . lng $ c)
            }
  pure $ catMaybes maybePositions
  where
    findHouse = housePosition houses'

houses :: ObliquityInformation -> [HouseCusp] -> [House]
houses obliquity =
  zipWith (curry buildHouse) [I .. XII]
  where
    buildHouse (n, c) =
      House n (Longitude c) (declination equatorial)
      where
        equatorial = eclipticToEquatorial obliquity (mkEcliptic {lng = c})


---
--- DATE HELPERS
---

-- | Given an @InputLocation@, produce a @Location@ if the coordinates are valid.
