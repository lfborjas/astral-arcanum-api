{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Effects.TimeZoneData where

import Control.Algebra
import Control.Carrier.Reader
import Data.Time.LocalTime.TimeZone.Olson
import Data.Time.LocalTime.TimeZone.Series
import qualified Data.Time.LocalTime.TimeZone.Detect as TZD
import Import 

data TimeZoneData (m :: Type -> Type) k where
  TimeAtPointToUTC :: Double -> Double -> LocalTime -> TimeZoneData m UTCTime
  TimeZoneAtPoint  :: Double -> Double -> LocalTime -> TimeZoneData m (TZD.TimeZoneName, TimeZone) 
  TimeZoneFromName :: TZD.TimeZoneName -> UTCTime -> TimeZoneData m TimeZone
  CurrentTimeUTC   :: TimeZoneData m UTCTime

timeAtPointToUTC :: Has TimeZoneData sig m => Double -> Double -> LocalTime -> m UTCTime
timeAtPointToUTC lt lg localTime = send $ TimeAtPointToUTC lt lg localTime

timeZoneAtPoint :: Has TimeZoneData sig m => Double -> Double -> LocalTime -> m (TZD.TimeZoneName, TimeZone)
timeZoneAtPoint lt lg localTime = send $ TimeZoneAtPoint lt lg localTime

timeZoneFromName :: Has TimeZoneData sig m => TZD.TimeZoneName -> UTCTime -> m TimeZone
timeZoneFromName tzn ut = send $ TimeZoneFromName tzn ut

currentTimeUTC :: Has TimeZoneData sig m => m UTCTime
currentTimeUTC = send CurrentTimeUTC

-- | IO carrier for the TimeZoneData algebra
newtype TimeZoneDataIOC m a = TimeZoneDataIOC {runTimeZoneDataIO :: ReaderC TZD.TimeZoneDatabase m a}
  deriving (Applicative, Functor, Monad, MonadIO, MonadFail)

runTimeZoneDataWithDB :: TZD.TimeZoneDatabase -> TimeZoneDataIOC m a -> m a
runTimeZoneDataWithDB db = runReader db . runTimeZoneDataIO

{- ORMOLU_DISABLE -}
instance (MonadIO m, Algebra sig m)
  => Algebra (TimeZoneData :+: sig) (TimeZoneDataIOC m) where
  alg hdl sig ctx = TimeZoneDataIOC $ case sig of
    L CurrentTimeUTC -> (<$ ctx) <$> liftIO getCurrentTime
    L (TimeAtPointToUTC lt lg localTime) -> do
      db <- ask
      (<$ ctx) <$> liftIO (TZD.timeAtPointToUTC db lt lg localTime)
    L (TimeZoneAtPoint lt lg localTime) -> do
      db <- ask
      (<$ ctx) <$> liftIO (timeZoneAtMoment db lt lg localTime)
    L (TimeZoneFromName tzName utReference) -> do
      (<$ ctx) <$> liftIO (findTimeZoneFromName tzName utReference)
    R other -> alg (runTimeZoneDataIO . hdl) (R other) ctx
{- ORMOLU_DISABLE -}

withTimeZoneData :: FilePath -> (TZD.TimeZoneDatabase -> IO a) -> IO a
withTimeZoneData = TZD.withTimeZoneDatabase


---
--- EXTRAS
--- 

timeZoneAtMoment :: TZD.TimeZoneDatabase -> Double -> Double -> LocalTime -> IO (TZD.TimeZoneName, TimeZone)
timeZoneAtMoment database lat lng referenceTime = do
  tzName <- TZD.lookupTimeZoneName database lat lng
  tzSeries <- getTimeZoneSeriesFromOlsonFile $ "/usr/share/zoneinfo/" <> tzName
  let utTime = localTimeToUTC' tzSeries referenceTime
  pure (tzName, timeZoneFromSeries tzSeries utTime)

timeZoneAtMomentUTC :: TZD.TimeZoneDatabase -> Double -> Double -> UTCTime -> IO (TZD.TimeZoneName, TimeZone)
timeZoneAtMomentUTC database lat lng referenceTimeUTC = do
  tzName <- TZD.lookupTimeZoneName database lat lng
  tzSeries <- getTimeZoneSeriesFromOlsonFile $ "/usr/share/zoneinfo/" <> tzName
  pure (tzName, timeZoneFromSeries tzSeries referenceTimeUTC)
  
findTimeZoneFromName :: TZD.TimeZoneName -> UTCTime -> IO TimeZone
findTimeZoneFromName tzName referenceTimeUTC = do
  tzSeries <- getTimeZoneSeriesFromOlsonFile $ "/usr/share/zoneinfo/" <> tzName
  pure $ timeZoneFromSeries tzSeries referenceTimeUTC
