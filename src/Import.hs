module Import
  ( module Relude
  , module Relude.Extra.Newtype
  , module Control.Algebra
  , module Control.Carrier.Reader
  , module Data.Time
  , Throw
  , throwError
  , headMaybe
  , lastMaybe
  , camelToSnake
  , dropPrefix
  , between
  , maybeBetween
  , rotateList
  -- TODO(luis) move these extras, for testing:
  , runError, runM
  ) where

-- we hide `Reader` and other mtl-isms in favor of `fused-effects` implementations
import Relude hiding (ask, runReader, Reader)
import Relude.Extra.Newtype

import Control.Algebra
import Control.Carrier.Reader (runReader, ReaderC, ask, Reader)
import Control.Carrier.Error.Either (runError, Throw, throwError)
import Control.Carrier.Lift (runM)
import Data.Aeson.Types (camelTo2)
import Data.Time


-- | total version of `head`
headMaybe :: [b] -> Maybe b
headMaybe = viaNonEmpty head

-- | total version of `last`
lastMaybe :: [b] -> Maybe b
lastMaybe = viaNonEmpty last

camelToSnake :: String -> String
camelToSnake = camelTo2 '_'

dropPrefix :: String -> String -> String
dropPrefix p = drop (length $ p <> "_") . camelToSnake

between :: Ord a => (a, a) -> a -> Bool
between (begin, end) x =
  x >= begin && x <= end

maybeBetween :: Ord a => (a, a) -> a -> Maybe a
maybeBetween range x =
  if between range x then Just x else Nothing

-- from: https://stackoverflow.com/questions/16378773/rotate-a-list-in-haskell
rotateList :: Int -> [a] -> [a]
rotateList _ [] = []
rotateList n xs = zipWith const (drop n (cycle xs)) xs
