{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Effects.Email (
  module Network.Api.Postmark,
  EmailRelay(..),
  EmailRelayIOC(..),
  sendEmailWithTemplate,
  runEmailWithToken,
  -- | helpers
  --passwordResetEmailTemplate,
  --passwordResetModel
  passwordResetEmail
) where

import Import
import Network.Api.Postmark hiding (email)

import qualified Data.Map as M

-- | Default for password reset, uses the template we created at
-- https://account.postmarkapp.com/servers/6462117/templates/23343616/edit
passwordResetEmailTemplate :: EmailWithTemplate
passwordResetEmailTemplate = defaultEmailWithTemplate {
  templateId = 23343616
, emailFrom' = "info@astralarcanum.com"
}

passwordResetModel :: Text -> Text -> Map Text Text
passwordResetModel userEmail code =
  M.fromList [("reset_code", code), ("reset_link", link)]
  where
    link = "https://astralarcanum.app/reset-password?email=" <> userEmail <> "&code=" <> code

passwordResetEmail :: Text -> Text -> EmailWithTemplate
passwordResetEmail userEmail code =
  passwordResetEmailTemplate{
    templateModel = passwordResetModel userEmail code
  , emailTo' = [userEmail]
  }

-- algebra

data EmailRelay (m :: Type -> Type) k where
  SendEmailWithTemplate :: EmailWithTemplate -> EmailRelay m (Either Text Sent)

sendEmailWithTemplate :: Has EmailRelay sig m => EmailWithTemplate -> m (Either Text Sent)
sendEmailWithTemplate = send . SendEmailWithTemplate

-- carrier
newtype EmailRelayIOC m a = EmailRelayIOC { runEmailIO :: ReaderC PostmarkApiToken m a}
  deriving (Applicative, Functor, Monad, MonadIO, MonadFail)

runEmailWithToken :: PostmarkApiToken -> EmailRelayIOC m hs -> m hs
runEmailWithToken token = runReader token . runEmailIO

instance
  (MonadIO m, Algebra sig m) =>
  Algebra (EmailRelay :+: sig) (EmailRelayIOC m)
  where
    alg hdl sig ctx = EmailRelayIOC $ case sig of
      L (SendEmailWithTemplate et) -> do
        token <- ask
        response <- liftIO $ request (postmarkHttps token) $ emailWithTemplate et
        case response of
          PostmarkSuccess a -> pure ((<$ ctx) (Right a))
          PostmarkFailure (PostmarkError _etype msg) -> pure ((<$ ctx) (Left msg))
          PostmarkUnauthorized -> do
            --log $ Error "Invalid postmark credentials"
            pure ((<$ ctx) (Left "Error sending email"))
          PostmarkUnexpected {} -> do
            --log $ Error $ "Unexpected error from postmark" <> show u
            pure ((<$ ctx) (Left "Unexpected error sending email"))
      R other -> alg (runEmailIO . hdl) (R other) ctx
