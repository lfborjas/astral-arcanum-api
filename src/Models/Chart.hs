{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE DeriveFunctor #-}

module Models.Chart where

import Import
import Data.Profunctor.Product.TH (makeAdaptorAndInstanceInferrable)
import Opaleye
import Database.Enum
    ( EnumMapper(enumToFields, enumFromField), enumMapper )
import Database.Orphanage ()
import Models.Common
import Data.UUID
import Opaleye.Internal.Inferrable
import Data.Profunctor.Product.Default (Default(..))
import Models.UserAccount
import Ephemeris.Types (Latitude(..), Longitude(..))
import Data.Time.LocalTime.TimeZone.Detect (TimeZoneName)
---
--- Supporting Types
---

newtype ChartID' a = ChartID a
  deriving newtype (Eq, Show)
  deriving Functor

$(makeAdaptorAndInstanceInferrable  "pChartID" ''ChartID')

type ChartIDField = ChartID' (Field SqlInt8)
type ChartIDWrite = ChartID' (Maybe (Field SqlInt8))
type ChartID = ChartID' Int64

-----------------------------------------------------------------------------

newtype ChartExternalID' a = ChartExternalID { getChartExternalId :: a}
  deriving newtype (Eq, Show)
  deriving Functor

$(makeAdaptorAndInstanceInferrable "pChartExternalID" ''ChartExternalID')

type ChartExternalIDField = ChartExternalID' (Field SqlUuid)
type ChartExternalIDWrite = ChartExternalID' (Maybe (Field SqlUuid))
type ChartExternalID = ChartExternalID' UUID

-----------------------------------------------------------------------------

data SqlChartType
data ChartType = Natal | Event

toSqlChartTypeString :: ChartType -> String
toSqlChartTypeString = \case
  Natal -> "Natal"
  Event -> "Event"

fromSqlChartTypeString :: String -> Maybe ChartType
fromSqlChartTypeString = \case
  "Natal" -> Just Natal
  "Event" -> Just Event
  _ -> Nothing

sqlChartTypeMapper :: EnumMapper sqlEnum ChartType
sqlChartTypeMapper = enumMapper "chart_type" fromSqlChartTypeString toSqlChartTypeString

instance DefaultFromField SqlChartType ChartType where
  defaultFromField = enumFromField sqlChartTypeMapper

instance chartType ~ ChartType
  => Default (Inferrable FromFields) (Column SqlChartType) chartType where
  def = Inferrable def

instance Default ToFields ChartType (Column SqlChartType) where
  def = enumToFields sqlChartTypeMapper

-----------------------------------------------------------------------------

---
--- ORPHANS
---
instance DefaultFromField SqlFloat8 Latitude where
  defaultFromField = Latitude <$> defaultFromField

-- NOTE(luis) not providing an inferrable instance because we can't
-- just assume that any ol' float is a latitude or longitude!

instance Default ToFields Latitude (Column SqlFloat8) where
  def = toToFields (\(Latitude lt) -> sqlDouble lt)

-----------------------------------------------------------------------------

instance DefaultFromField SqlFloat8 Longitude where
  defaultFromField = Longitude <$> defaultFromField

instance Default ToFields Longitude (Column SqlFloat8) where
  def = toToFields (\(Longitude lt) -> sqlDouble lt)


---
--- Actual Model
---

data Chart' cid ceid cuid cip ct cprim cct cln cm clat clng ctz =
  Chart
    { chartID :: cid
    , chartExternalID :: ceid
    , chartUserAccountID :: cuid
    , chartIsPublic :: cip
    , chartTitle :: ct
    , chartIsPrimary :: cprim
    , chartType :: cct
    , chartLocationName :: cln
    , chartMoment :: cm
    , chartLatitude :: clat
    , chartLongitude :: clng
    , chartTimeZoneName :: ctz
    }

type Chart =
  Entity
    (Chart'
      ChartID
      ChartExternalID
      UserID
      Bool
      (Maybe Text)
      Bool
      ChartType
      (Maybe Text)
      UTCTime
      Double--Latitude
      Double--Longitude
      Text--TimeZoneName
      )

type ChartWrite =
    EntityWriteField
      (Chart'
        ChartIDWrite
        ChartExternalIDWrite
        UserIDField
        (Field SqlBool)
        (FieldNullable SqlText)
        (Field SqlBool)
        (Field SqlChartType)
        (FieldNullable SqlText)
        (Field SqlTimestamptz)
        (Field SqlFloat8)
        (Field SqlFloat8)
        (Field SqlText))

type ChartField  =
    EntityField
      (Chart'
        ChartIDField
        ChartExternalIDField
        UserIDField
        (Field SqlBool)
        (FieldNullable SqlText)
        (Field SqlBool)
        (Field SqlChartType)
        (FieldNullable SqlText)
        (Field SqlTimestamptz)
        (Field SqlFloat8)
        (Field SqlFloat8)
        (Field SqlText))

$(makeAdaptorAndInstanceInferrable "pChart" ''Chart')

savedChartTable ::
  Table ChartWrite ChartField
savedChartTable =
  table "chart" . pEntity . withTimestampFields $
    pChart
      Chart
        { chartID = pChartID (ChartID $ tableField "id")
        , chartExternalID = pChartExternalID $ ChartExternalID $ tableField "external_id"
        , chartUserAccountID = pUserID $ UserID $ tableField "user_account_id"
        , chartIsPublic = tableField "is_public"
        , chartTitle = tableField "title"
        , chartIsPrimary = tableField "is_primary"
        , chartType = tableField "chart_type"
        , chartLocationName = tableField "location_name"
        , chartMoment = tableField  "moment"
        , chartLatitude = tableField "latitude"
        , chartLongitude = tableField "longitude"
        , chartTimeZoneName = tableField "timezone_name"
        }

---
--- QUERIES
---

-- TODO: use `named`?
data NewChart = NewChart
  { newChartUserAccountID :: UserID
  , newChartIsPublic :: Bool
  , newChartTitle :: Maybe Text
  , newChartIsPrimary :: Bool
  , newChartType :: ChartType
  , newChartLocationName :: Maybe Text
  , newChartMoment :: UTCTime
  , newChartLatitude :: Latitude
  , newChartLongitude :: Longitude
  , newChartTimeZoneName :: TimeZoneName
  }

newChart :: NewChart -> Insert [Chart]
newChart NewChart{newChartUserAccountID, newChartIsPublic, newChartTitle, newChartIsPrimary, newChartType, newChartLocationName, newChartMoment, newChartLatitude, newChartLongitude, newChartTimeZoneName} =
  Insert
    { iTable = savedChartTable
    , iRows = withTimestamp [row]
    , iReturning = rReturning id--entity
    , iOnConflict = Just DoNothing
    }
  where
    row =
      Chart
        { chartID = ChartID Nothing
        , chartExternalID = ChartExternalID Nothing
        , chartUserAccountID = toFields newChartUserAccountID
        , chartIsPublic = toFields newChartIsPublic
        , chartTitle = toFields newChartTitle
        , chartIsPrimary = toFields newChartIsPrimary
        , chartType = toFields newChartType
        , chartLocationName = toFields newChartLocationName
        , chartMoment = toFields newChartMoment
        , chartLatitude = toFields newChartLatitude
        , chartLongitude = toFields newChartLongitude
        , chartTimeZoneName = toFields newChartTimeZoneName
        }

data UpdateChart = UpdateChart
  { updateChartIsPublic :: Maybe Bool
  , updateChartTitle :: Maybe Text
  , updateChartIsPrimary :: Maybe Bool
  , updateChartType :: Maybe ChartType
  }

updateChart :: UserID -> ChartExternalID -> UpdateChart -> Update [Chart]
updateChart uid eid UpdateChart{updateChartIsPublic, updateChartTitle, updateChartIsPrimary, updateChartType} =
  Update
    { uTable = savedChartTable
    , uUpdateWith = updateRecordWith updateChartData
    , uWhere = ownChart uid eid
    , uReturning = rReturning id
    }
  where
    updateChartData chartEntity =
      chartEntity{
        chartID = Just <$> chartID chartEntity
      , chartExternalID = Just <$> chartExternalID chartEntity
      , chartIsPublic = maybe (chartIsPublic chartEntity) toFields updateChartIsPublic
      , chartTitle = maybe (chartTitle chartEntity) (toFields . Just) updateChartTitle
      , chartIsPrimary = maybe (chartIsPrimary chartEntity) toFields updateChartIsPrimary
      , chartType = maybe (chartType chartEntity) toFields updateChartType
      }

deleteChart :: UserID -> ChartExternalID -> Delete [Chart]
deleteChart uid eid =
  Delete
    { dTable = savedChartTable
    , dWhere = ownChart uid eid
    , dReturning = rReturning id 
    }

ownChart :: UserID -> ChartExternalID -> ChartField -> Field SqlBool
ownChart uid eid Entity{record} =
  chartExternalID record .=== toFields eid .&& chartUserAccountID record .=== toFields uid
--
-- QUERIES
--

-- | Get all charts for a given user, regardless of ownership.
-- Meant to be called in the context of the owner querying them!
chartsByUser :: UserID -> Select ChartField
chartsByUser uid = do
  charts <- selectTable savedChartTable
  where_ $ (chartUserAccountID . record $ charts) .=== toFields uid
  pure charts

-- | Only public charts for a given user.
publicChartsByUser :: UserID -> Select ChartField
publicChartsByUser uid = do
  userCharts <- chartsByUser uid
  where_ $ (chartIsPublic . record $ userCharts) .=== toFields True
  pure userCharts

-- | Find a chart given its external id.
chartByExternalID :: ChartExternalID -> Select ChartField
chartByExternalID eid = do
  charts <- selectTable savedChartTable
  where_ $ (chartExternalID . record $ charts) .=== toFields eid
  pure charts

-- | Given an optional user, and a chart id, return the chart
-- if the chart is public or the user is the owner.
publicOrOwnChart :: Maybe UserID -> ChartExternalID -> Select ChartField
publicOrOwnChart uid eid = do
  chart@Entity{record} <- chartByExternalID eid
  --where_ $ ifThenElse (chartIsPublic record) (sqlBool True) (chartUserAccountID record .=== toFields uid)
  let isPublic = chartIsPublic record
      isOwn = maybe (sqlBool False) (\u -> chartUserAccountID record .=== toFields u) uid
  where_ $ isPublic .|| isOwn
  pure chart

defaultChartForUser :: UserID -> Select ChartField
defaultChartForUser uid = do
  charts@Entity{record} <- selectTable savedChartTable
  where_ $ chartUserAccountID record .=== toFields uid .&& chartIsPrimary record
  pure charts
