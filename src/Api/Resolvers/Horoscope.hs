{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RankNTypes #-}

{-# LANGUAGE RecordWildCards #-}
module Api.Resolvers.Horoscope where

import qualified Api.DomainUtils as Domain
import Api.Error
import qualified Api.Schema as API
import Control.Carrier.Error.Either
import Data.Fixed (Fixed (MkFixed))
import Data.Morpheus.Types
import Data.Time.Format.ISO8601 (iso8601ParseM, iso8601Show)
import qualified Ephemeris as Domain
import Import hiding (second)
import Effects
import qualified Models.Chart as DB
import Api.Resolvers.Common
import Database.Orphanage ()
import Models.Common
import qualified Data.Morpheus.Types as Morpheus
import qualified Data.UUID as UUID
import Data.Time.LocalTime.TimeZone.Detect (TimeZoneName)
import Data.Time.Clock.POSIX (utcTimeToPOSIXSeconds)

resolveMoment :: API.AppM sig m => API.MomentArgs -> ResolverQ () m API.Moment
resolveMoment API.MomentArgs{API.utcDate} = do
  parsedTime <- lift $ parseZonedTime utcDate
  moment' <- lift $ Domain.moment parsedTime
  pure $
    API.Moment
      (resolvePlanetPositions $ Domain.momentPlanetPositions moment')
      (resolveAspects (Domain.momentPlanetaryAspects moment', []))

resolvePrimarySavedChart :: API.AppM sig m => ComposedResolver o () m Maybe API.SavedChart
resolvePrimarySavedChart = do
  currentUserId <- lift requireAuth
  chart <- lift $ select $ DB.defaultChartForUser currentUserId
  case headMaybe chart of
    Nothing -> pure Nothing
    Just savedChart@(Entity DB.Chart{} _ _) ->
      Just <$> resolveSavedChart savedChart

resolveOwnSavedCharts :: API.AppM sig m => ComposedResolver o () m [] API.SavedChart
resolveOwnSavedCharts = do
  currentUserId <- lift requireAuth
  charts <- lift $ select $ DB.chartsByUser currentUserId
  traverse resolveSavedChart charts

resolveChart :: API.AppM sig m => API.SavedChartArgs -> ComposedResolver o () m Maybe API.SavedChart
resolveChart (API.SavedChartArgs mid) = do
  currentUserId <- lift currentUser
  chartId <- lift $ parseChartID mid
  chart <- lift $ select $ DB.publicOrOwnChart currentUserId chartId
  case headMaybe chart of
    Nothing -> pure Nothing
    Just savedChart -> Just <$> resolveSavedChart savedChart

resolveSaveChart :: API.AppM sig m => API.SaveChartArgs -> ComposedResolver o () m Maybe API.SavedChart
resolveSaveChart (API.SaveChartArgs API.NewSavedChart{..})= do
  let (API.Location lt lng locName) = newChartLocation

  currentUserId <- lift requireAuth
  parsedTime <- lift $ parseDateTime newChartDate
  (tzName, tz) <- lift $ timeZoneAtPoint lt lng parsedTime

  chart <- lift $ insert $ DB.newChart $
    DB.NewChart{
      DB.newChartUserAccountID = currentUserId
    , DB.newChartIsPublic = newChartIsPublic
    , DB.newChartTitle = newChartTitle
    , DB.newChartIsPrimary = newChartIsPrimary
    , DB.newChartType = mappedType newChartType
    , DB.newChartLocationName = locName
    , DB.newChartMoment = zonedTimeToUTC $ ZonedTime parsedTime tz
    , DB.newChartLatitude = Domain.Latitude lt
    , DB.newChartLongitude = Domain.Longitude lng
    , DB.newChartTimeZoneName = tzName
    }
  case headMaybe chart of
    Nothing -> pure Nothing
    Just savedChart -> Just <$> resolveSavedChart savedChart

mappedType :: API.ChartType -> DB.ChartType
mappedType = \case
  API.Natal -> DB.Natal
  API.Event -> DB.Event

-- see: https://github.com/morpheusgraphql/morpheus-graphql/blob/ffa8a101624a217d43b47f86149dbc1452915251/morpheus-graphql-core/src/Data/Morpheus/Types/ID.hs
parseChartID :: (Has (Throw ServerError) sig m) => Morpheus.ID -> m DB.ChartExternalID
parseChartID (Morpheus.ID cid) =
  case UUID.fromText cid of
    Nothing -> throwError $ serverError "Invalid Chart ID"
    Just uuid -> pure $ DB.ChartExternalID  uuid

resolveUpdateChart :: API.AppM sig m => API.UpdateChartArgs -> ComposedResolver o () m Maybe API.SavedChart
resolveUpdateChart (API.UpdateChartArgs mid API.UpdateSavedChart{..}) = do
  currentUserId <- lift requireAuth
  chartId <- lift $ parseChartID mid
  chart <- lift $ update $ DB.updateChart currentUserId chartId $
    DB.UpdateChart {
      DB.updateChartIsPublic = updateIsPublic
    , DB.updateChartTitle = updateTitle
    , DB.updateChartIsPrimary = updateIsPrimary
    , DB.updateChartType = mappedType <$> updateChartType
    }
  case headMaybe chart of
    Nothing -> pure Nothing
    Just updatedChart -> Just <$> resolveSavedChart updatedChart

resolveDeleteChart :: API.AppM sig m => API.DeleteChartArgs -> ComposedResolver o () m Maybe API.SavedChart
resolveDeleteChart (API.DeleteChartArgs mid) = do
  currentUserId <- lift requireAuth
  chartId <- lift $ parseChartID mid
  chart <- lift $ delete $ DB.deleteChart currentUserId chartId
  case headMaybe chart of
    Nothing -> pure Nothing
    Just deletedChart -> Just <$> resolveSavedChart deletedChart

resolveHoroscope :: API.AppM sig m => API.HoroscopeArgs -> ResolverQ () m API.Horoscope
resolveHoroscope API.HoroscopeArgs {API.location, API.date} = do
  parsedLocation@(Domain.Location _ (Domain.Latitude lt) (Domain.Longitude lng)) <-
    lift $ parseLocation location
  parsedTime <- lift $ parseDateTime date
  (_tzUnixName, tz) <- lift $ timeZoneAtPoint lt lng parsedTime
  let zonedTime = ZonedTime parsedTime tz
  resolveHoroscopeParsed parsedLocation zonedTime

---
--- SUB-RESOLVERS
---

resolveHoroscopeParsed :: API.AppM sig m => Domain.Location -> ZonedTime -> ResolverO o () m API.Horoscope
resolveHoroscopeParsed parsedLocation zonedTime = do
  horoscope' <- lift $ Domain.horoscope $ Domain.BirthData parsedLocation zonedTime
  pure $
    API.Horoscope
      (resolveHouses $ Domain.horoscopeHouses horoscope')
      (resolvePlanetPositions $ Domain.horoscopePlanetPositions horoscope')
      (resolveSystemUsed $ Domain.horoscopeSystem horoscope')
      (resolveAspects (Domain.horoscopePlanetaryAspects horoscope', Domain.horoscopeAxisAspects horoscope'))
      (resolveHoroscopeSummary (Domain.horoscopePlanetPositions  horoscope') (Domain.horoscopeHouses horoscope'))

resolveHoroscopeSummary :: API.AppM sig m => [Domain.PlanetPosition] -> [Domain.House] -> ResolverO o () m API.HoroscopeSummary
resolveHoroscopeSummary planets houses =
  let
    sunLongitude = Domain.findPlanet Domain.Sun planets & resolveLongitude'
    moonLongitude = Domain.findPlanet Domain.Moon planets & resolveLongitude'
    ascLongitude = Domain.findAscendant houses & resolveLongitude'
  in pure $ API.HoroscopeSummary {..}
  where
    resolveLongitude' = \case
      Nothing -> pure Nothing
      Just l -> Just <$> resolveLongitude l

resolveSavedChart :: API.AppM sig m => DB.Chart -> ResolverO o () m API.SavedChart
resolveSavedChart (Entity DB.Chart{..} createdAt updatedAt) = do
  -- NOTE(luis) have to re-find the TZ because PG _actually_ forgets it
  tz <- lift $ timeZoneFromName (toString chartTimeZoneName) chartMoment
  let zonedMoment = utcToZonedTime tz chartMoment
      utcZoned = utcToZonedTime utc
  pure $
    API.SavedChart
      (pure . resolveID . DB.getChartExternalId $ chartExternalID)
      (pure chartIsPublic)
      (pure chartTitle)
      (pure chartIsPrimary)
      (pure . mapChartType $ chartType)
      (resolveInstant zonedMoment (Just . toString $ chartTimeZoneName))
      (pure chartLocationName)
      (resolveDegrees chartLatitude)
      (resolveDegrees chartLongitude)
      (resolveInstant (utcZoned createdAt) Nothing)
      (resolveInstant (utcZoned updatedAt) Nothing)
      (resolveHoroscopeParsed domainLocation zonedMoment)
  where
    mapChartType = \case
      DB.Natal -> API.Natal
      DB.Event -> API.Event
    domainLocation = Domain.Location chartLocationName (Domain.Latitude chartLatitude) (Domain.Longitude chartLongitude)

resolveID :: UUID.UUID -> Morpheus.ID
resolveID = UUID.toString >>> toText >>> Morpheus.ID

resolveInstant :: API.AppM sig m => ZonedTime -> Maybe TimeZoneName -> ResolverO o () m API.Instant
resolveInstant zt@(ZonedTime (LocalTime localDay time) timeZone) tzName =
  let
    (ye,mo,da) = toGregorian localDay
    year = pure . fromIntegral $ ye
    month = pure mo
    day = pure da
    hour = pure . todHour $ time
    minute = pure . todMin $ time
    -- TODO(luis) should the second fraction also be returned?
    (second', _secondFraction) = properFraction . todSec $ time
    second = pure second'
    timezoneName = pure $ toText $ fromMaybe (timeZoneName timeZone) tzName
    timezoneOffset = pure $ timeZoneMinutes timeZone
    epochMillis = pure $ floor . (1e3 *) . nominalDiffTimeToSeconds . utcTimeToPOSIXSeconds . zonedTimeToUTC $ zt
    formatted = pure $ toText . iso8601Show $ zt
  in pure $ API.Instant {..}


resolveHouses :: API.AppM sig m => [Domain.House] -> ComposedResolver o () m [] API.HouseCusp
resolveHouses = traverse houseResolver

houseResolver :: API.AppM sig m => Domain.House -> ResolverO o () m API.HouseCusp
houseResolver Domain.House {Domain.houseNumber, Domain.houseCusp, Domain.houseDeclination} =
  pure $
    API.HouseCusp
      (resolveHouseNumber houseNumber)
      (resolveLongitude houseCusp)
      (resolveDegrees houseDeclination)

resolvePlanetPositions :: API.AppM sig m => [Domain.PlanetPosition] -> ComposedResolver o () m [] API.PlanetPosition
resolvePlanetPositions = traverse planetResolver

planetResolver :: API.AppM sig m => Domain.PlanetPosition -> ResolverO o () m API.PlanetPosition
planetResolver Domain.PlanetPosition {Domain.planetName, Domain.planetLat, Domain.planetLng, Domain.planetLngSpeed, Domain.planetDeclination, Domain.planetHouse} =
  pure $
    API.PlanetPosition
       (pure . mapPlanetName $ planetName)
       huis
       (resolveDegrees . Domain.unLatitude $ planetLat)
       (resolveLongitude planetLng)
       (resolveDegrees planetLngSpeed)
       (resolveDegrees planetDeclination)
  where
    huis =
      case planetHouse of
        Nothing -> pure Nothing
        Just Domain.House {Domain.houseNumber} -> Just <$> resolveHouseNumber houseNumber
    mapPlanetName =
      \case
          Domain.Sun -> API.Sun
          Domain.Moon -> API.Moon
          Domain.Mercury -> API.Mercury
          Domain.Venus -> API.Venus
          Domain.Mars -> API.Mars
          Domain.Jupiter -> API.Jupiter
          Domain.Saturn -> API.Saturn
          Domain.Uranus -> API.Uranus
          Domain.Neptune -> API.Neptune
          Domain.Pluto -> API.Pluto
          Domain.MeanNode -> API.MeanNode
          Domain.TrueNode -> API.TrueNode
          Domain.MeanApog   -> API.Lilith
          Domain.OscuApog -> API.OscuApog
          Domain.Chiron -> API.Chiron
          _ -> API.Earth

resolveAspects :: API.AppM sig m => ([Domain.PlanetaryAspect], [Domain.AxisAspect]) -> ComposedResolver o () m [] API.Aspect
resolveAspects (p,a) =
  traverse planetaryAspectResolver p <> traverse axisAspectResolver a

planetaryAspectResolver :: API.AppM sig m => Domain.PlanetaryAspect -> ResolverO o () m API.Aspect
planetaryAspectResolver Domain.HoroscopeAspect {Domain.aspect, Domain.bodies, Domain.aspectAngle} =
  pure $
    API.Aspect
      (resolveAspectDefinition aspect)
      (resolveAspectPhase aspectAngle)
      (resolveDegrees . Domain.aspectAngleOrb $ aspectAngle)
      (resolveAspectingPlanet . fst $ bodies)
      (resolveAspectingPlanet . snd $ bodies)

axisAspectResolver :: API.AppM sig m => Domain.AxisAspect -> ResolverO o () m API.Aspect
axisAspectResolver Domain.HoroscopeAspect {Domain.aspect, Domain.bodies, Domain.aspectAngle} =
  pure $
    API.Aspect
      (resolveAspectDefinition aspect)
      (resolveAspectPhase aspectAngle)
      (resolveDegrees . Domain.aspectAngleOrb $ aspectAngle)
      (resolveAspectingPlanet . fst $ bodies)
      (resolveAspectedAxis . snd $ bodies)

resolveAspectedAxis :: API.AppM sig m => Domain.House -> ResolverO o () m API.AspectParticipant
resolveAspectedAxis pos = API.AspectParticipantHouseCusp <$> houseResolver pos

resolveAspectingPlanet :: API.AppM sig m => Domain.PlanetPosition -> ResolverO o () m API.AspectParticipant
resolveAspectingPlanet pos = API.AspectParticipantPlanetPosition <$> planetResolver pos

resolveAspectPhase :: API.AppM sig m => Domain.AspectAngle -> ResolverO o () m API.AspectPhase
resolveAspectPhase Domain.AspectAngle {Domain.aspectAngleApparentPhase} =
  pure $ case aspectAngleApparentPhase of
    Domain.Applying -> API.Applying
    Domain.Separating -> API.Separating
    Domain.Exact -> API.Exact

resolveAspectDefinition :: API.AppM sig m => Domain.Aspect -> ResolverO o () m API.AspectDefinition
resolveAspectDefinition Domain.Aspect{Domain.aspectName, Domain.maxOrb, Domain.angle, Domain.temperament, Domain.aspectType} =
  pure $
    API.AspectDefinition
      (pure . mapAspectName $ aspectName)
      (pure maxOrb)
      (pure angle)
      (pure . mapTemperament $ temperament)
      (pure . mapClassification $ aspectType)
  where
    mapAspectName =
      \case
          Domain.Conjunction -> API.Conjunction
          Domain.Sextile -> API.Sextile
          Domain.Square -> API.Square
          Domain.Trine -> API.Trine
          Domain.Opposition -> API.Opposition
          Domain.Quincunx -> API.Quincunx
          Domain.SemiSextile -> API.SemiSextile
          Domain.Quintile -> API.Quintile
          Domain.BiQuintile -> API.BiQuintile
          Domain.Septile -> API.Septile
          Domain.SemiSquare -> API.SemiSquare
          Domain.Novile -> API.Novile
          Domain.Sesquisquare -> API.Sesquisquare
    mapTemperament =
      \case
        Domain.Analytical -> API.Analytical
        Domain.Synthetic -> API.Synthetic
        Domain.Neutral -> API.Neutral
    mapClassification =
      \case
        Domain.Major -> API.Major
        Domain.Minor -> API.Minor



resolveHouseNumber :: API.AppM sig m => Domain.HouseNumber -> ResolverO o () m API.HouseNumber
resolveHouseNumber hausNumber =
  pure $ API.HouseNumber num lbl
  where
    num = pure . (+ 1) . fromEnum $ hausNumber
    lbl = pure . toText . Domain.label $ hausNumber

resolveLongitude :: API.AppM sig m => Domain.Longitude -> ResolverO o () m API.SplitDegrees
resolveLongitude (Domain.Longitude lng) =
  pure $ API.SplitDegrees zdc dg mn sc (pure lng)
  where
    split' = Domain.splitDegreesZodiac lng
    zdc = pure . resolveZodiacSign . Domain.longitudeZodiacSign $ split'
    dg = pure . fromIntegral . Domain.longitudeDegrees $ split'
    mn = pure . fromIntegral . Domain.longitudeMinutes $ split'
    sc = pure . fromIntegral . Domain.longitudeSeconds $ split'
    resolveZodiacSign Nothing = Nothing
    resolveZodiacSign (Just z) =
      Just $ case z of
        Domain.Aries -> API.Aries
        Domain.Taurus -> API.Taurus
        Domain.Gemini -> API.Gemini
        Domain.Cancer -> API.Cancer
        Domain.Leo -> API.Leo
        Domain.Virgo -> API.Virgo
        Domain.Libra -> API.Libra
        Domain.Scorpio -> API.Scorpio
        Domain.Sagittarius -> API.Sagittarius
        Domain.Capricorn -> API.Capricorn
        Domain.Aquarius -> API.Aquarius
        Domain.Pisces -> API.Pisces

resolveDegrees :: API.AppM sig m => Double -> ResolverO o () m API.SplitDegrees
resolveDegrees lng =
  pure $ API.SplitDegrees (pure Nothing) dg mn sc (pure lng)
  where
    split' = Domain.splitDegreesZodiac lng
    dg = pure . fromIntegral . Domain.longitudeDegrees $ split'
    mn = pure . fromIntegral . Domain.longitudeMinutes $ split'
    sc = pure . fromIntegral . Domain.longitudeSeconds $ split'

resolveSystemUsed :: API.AppM sig m => Domain.HouseSystem -> ResolverO o () m API.HouseSystem
resolveSystemUsed sys =
  pure $ case sys of
    Domain.Placidus -> API.Placidus
    Domain.Koch -> API.Koch
    Domain.Porphyrius -> API.Porphyrius
    Domain.Regiomontanus -> API.Regiomontanus
    Domain.Campanus -> API.Campanus
    Domain.Equal -> API.Equal
    Domain.WholeSign -> API.WholeSign

---
--- PARSE HELPERS
---

parseLocation :: (Has (Throw ServerError) sig m) => API.Location -> m Domain.Location
parseLocation (API.Location ilat ilng iname) = do
  case Domain.Location <$> Just iname <*> mkLatitude ilat <*> mkLongitude ilng of
    Nothing -> throwError $ serverError "Invalid coordinates"
    Just l -> pure l

parseZonedTime :: (Has (Throw ServerError) sig m, Has TimeZoneData sig m) => Maybe API.ZonedDateTime -> m UTCTime
parseZonedTime Nothing = currentTimeUTC
parseZonedTime (Just zt) =
  case parseFromString zt of
    Nothing -> throwError $ serverError "Invalid zoned time: must be an iso8601 string"
    Just t' -> pure t'
  where
    parseFromString API.ZonedDateTime {API.utcFormatted} = do
      asString <- toString <$> utcFormatted
      iso8601ParseM asString

-- | Given a @DateTime@, produce a @LocalTime@ from either its date components,
-- or the provided formatted string. The latter is supposed to be ISO8601 formatted.
parseDateTime :: (Has (Throw ServerError) sig m) => API.LocalDateTime -> m LocalTime
parseDateTime dt =
  case parseFromComponents dt <|> parseFromString dt of
    Nothing -> throwError $ serverError "Invalid date time: either date components or a valid ISO8601 string should be provided."
    Just t' -> pure t'
  where
    parseFromComponents API.LocalDateTime {..} = do
      y <- year
      m <- month
      d <- day
      hh <- hour
      mm <- minute
      ss <- second
      day' <- fromGregorianValid (fromIntegral y) m d
      time <- makeTimeOfDayValid hh mm (MkFixed $ fromIntegral ss)
      pure $ LocalTime day' time
    parseFromString API.LocalDateTime {API.formatted} = do
      asString <- toString <$> formatted
      iso8601ParseM asString

-- orphanage
mkLatitude :: Double -> Maybe Domain.Latitude
mkLatitude l =
  maybeBetween (-90.0, 90.0) l >>= (Just . Domain.Latitude)

mkLongitude :: Double -> Maybe Domain.Longitude
mkLongitude l =
  maybeBetween (-180.0, 180.0) l >>= (Just . Domain.Longitude)
