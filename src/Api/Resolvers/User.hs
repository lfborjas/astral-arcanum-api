{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RankNTypes #-}

module Api.Resolvers.User where

import Import
import Api.Error
import Effects
import Data.Morpheus.Types
import qualified Api.Schema as API
import qualified Models.UserAccount as DB
import Domain.Auth
import Data.Password.Argon2
import Data.Password.Validate
import Data.CaseInsensitive (CI (original))
import qualified Data.Text as T
import qualified Models.PasswordReset as DB
import Config (LogMessage(Info, Error))
import Data.Time.Format.ISO8601 (iso8601Show)
-- to be able to deduce SqlTimestampTz -> UTCTime in handler queries
import Database.Orphanage ()
import Api.Resolvers.Common (requireAuth)


resolveSignIn :: API.AppM sig m => API.SignInArgs -> ResolverM () m API.Session
resolveSignIn (API.SignInArgs creds) = do
  authResult <- lift $ doSignIn creds
  case authResult of
    Left e -> lift $ throwError $ serverError e
    Right (JWS tok, uname, uemail) ->
      pure $
        API.Session
          (pure tok)
          (pure $ API.UserProfile (pure uname) (pure uemail))

resolveSignUp :: API.AppM sig m => API.SignUpArgs -> ResolverM () m API.Session
resolveSignUp (API.SignUpArgs userInfo) = do
  signupResult <- lift $ doSignUp userInfo
  case signupResult of
    Left e -> lift $ throwError $ serverError e
    Right (JWS tok, uname, uemail) ->
      pure $
        API.Session
          (pure tok)
          (pure $ API.UserProfile (pure uname) (pure uemail))

resolveUpdateProfile :: API.AppM sig m => API.UpdateProfileArgs -> ResolverM () m API.UserProfile
resolveUpdateProfile (API.UpdateProfileArgs (API.UserProfileUpdate newName )) = do
  updateResult <- lift $ doProfileUpdate newName
  case updateResult of
    Left e -> lift $ throwError $ serverError e
    Right (uname, uemail) ->
      pure $ API.UserProfile (pure uname) (pure uemail)

resolveUpdateCredentials :: API.AppM sig m => API.UpdateLoginArgs  -> ResolverM () m API.UserProfile
resolveUpdateCredentials (API.UpdateLoginArgs currentPassword updates) = do
  updateResult <- lift $ doLoginUpdate currentPassword updates
  case updateResult of
    Left e -> lift $ throwError $ serverError e
    Right (uname, uemail) ->
      pure $ API.UserProfile (pure uname) (pure uemail)

-- | Request a password reset for the first time. Will produce a fake expiration (and logging)
-- if the user doesn't exist or if a request to reset password already exists.
resolveRequestPasswordReset :: API.AppM sig m => API.RequestPasswordResetArgs -> ResolverM () m API.ResetPasswordConfirmation
resolveRequestPasswordReset (API.RequestPasswordResetArgs email) = do
  resetResult <- lift $ doRequestPasswordReset email
  case resetResult of
    Left e -> lift $ throwError $ serverError e
    Right expires -> do
      let timeAsString = toText .iso8601Show <$> expires
      pure $ API.ResetPasswordConfirmation (pure email) (pure timeAsString)

resolveResetPassword :: API.AppM sig m => API.ResetPasswordArgs -> ResolverM () m API.Session
resolveResetPassword (API.ResetPasswordArgs email resetCode newPassword) = do
  resetResult <- lift $ doResetPassword email (mkPassword resetCode) (mkPassword newPassword)
  case resetResult of
    Left e -> lift $ throwError $ serverError e
    Right userId -> do
      dbUser <- lift $ select $ DB.userByID userId
      case headMaybe dbUser of
        -- NOTE(luis) in theory this would never be reached, since the
        -- resetPassword query will only succeed if an account exists... but
        -- there's always the possibility that between doing that and actually
        -- establishing a session, we've somehow misplaced it (or deleted
        -- as per GDPR/etc.)
        Nothing -> lift $ throwError $ serverError "User doesn't exist."
        Just userData -> do
          (JWS tok, uname, uemail) <- lift $ userDataToSessionData userData
          pure $
            API.Session
              (pure tok)
              (pure $ API.UserProfile (pure uname) (pure uemail))

resolveCurrentUser :: API.AppM sig m => ResolverQ () m API.UserProfile
resolveCurrentUser = do
  currentUserId <- lift requireAuth
  dbUser <- lift $ select $ DB.userByID currentUserId
  case headMaybe dbUser of
    Nothing -> lift $ throwError $ serverError "User doesn't exist."
    Just (_uid, uname, uemail) -> do
      pure $ API.UserProfile (pure uname) (pure . original $ uemail)


---
--- HELPERS
--- 

-- | Validate a password with the default policy:
-- * min length: 8 chars, max 64
-- * only ASCII characters
validatePW :: Password -> ValidationResult
validatePW = validatePassword defaultPasswordPolicy_

type SessionData = (JWS, Maybe Text, Text)

userDataToSessionData :: API.AppM sig m => (DB.UserID, Maybe Text, CI Text) -> m SessionData
userDataToSessionData (uid, uname, uemail) = do
  let claims = mkUserClaims uid
  jws <- signJWT claims
  pure (jws, uname, original uemail)


doSignIn :: API.AppM sig m => API.UserCredentials -> m (Either Text SessionData)
doSignIn (API.UserCredentials email password) = do
  dbUser <- select $ DB.userByEmailForLogin email
  case headMaybe dbUser of
    Nothing -> pure . Left $ "Invalid credentials."
    Just (uid, uname, uemail, pwHash) -> do
      case checkPassword (mkPassword password) (PasswordHash pwHash) of
        PasswordCheckFail -> pure . Left $ "Invalid credentials."
        PasswordCheckSuccess -> Right <$> userDataToSessionData (uid, uname, uemail)

doSignUp :: API.AppM sig m => API.NewUser -> m (Either Text SessionData)
doSignUp (API.NewUser name email password') = do
  let password = mkPassword password'
  case validatePW password of
      InvalidPassword reasons -> pure . Left $ catReasons reasons
      ValidPassword -> do
        hashedPassword <- hashPasswordArgon2 password
        inserted <- insert $ DB.newUser name email hashedPassword
        case headMaybe inserted of
          Nothing -> pure . Left $ "Email is already taken."
          Just (uid, uname, uemail) -> Right <$> userDataToSessionData (uid, uname, uemail)

doProfileUpdate :: API.AppM sig m => Maybe Text -> m (Either Text (Maybe Text, Text))
doProfileUpdate newName = do
  currentUserId <- requireAuth
  updated <- update $ DB.updateUserProfileInfo currentUserId newName
  case headMaybe updated of
    Nothing -> pure . Left $ "Unable to update profile"
    Just (updatedName, updatedEmail, _updatedAt) -> pure . Right $ (updatedName, original updatedEmail)

doLoginUpdate :: API.AppM sig m => Text -> API.UserCredentialsUpdate -> m (Either Text (Maybe Text, Text))
doLoginUpdate currentPassword' (API.UserCredentialsUpdate newEmail newPassword') = do
  let currentPassword = mkPassword currentPassword'
      newPassword     = mkPassword <$> newPassword'
  currentUserId <- requireAuth
  dbUser <- select $ DB.userCredsById currentUserId
  case headMaybe dbUser of
    Nothing -> pure . Left $ "Account not found."
    Just (_uid,  pwHash) -> do
      case checkPassword currentPassword (PasswordHash pwHash) of
        PasswordCheckFail -> pure . Left $ "Invalid credentials."
        PasswordCheckSuccess -> do
          case maybe ValidPassword validatePW newPassword of
            InvalidPassword reasons -> pure . Left $ catReasons reasons
            ValidPassword -> do
              hashedPassword <- case newPassword of
                Nothing -> pure Nothing
                Just np -> do 
                  hashed <- hashPasswordArgon2 np
                  pure $ Just hashed
              updated <- update $ DB.updateUserCreds currentUserId newEmail hashedPassword
              case headMaybe updated of
                Nothing -> pure . Left $ "Unable to update login credentials."
                Just (updatedName, updatedEmail, _updatedAt) -> pure . Right $ (updatedName, original updatedEmail)

doRequestPasswordReset ::
  API.AppM sig m
  => Text
  -> m (Either Text (Maybe UTCTime))
doRequestPasswordReset requestingEmail = do
  -- 1. generate a random password
  generatedPassword <- getRandomPassword
  hashedPw <- hashPasswordArgon2 . mkPassword $ generatedPassword
  -- 2. record (or refresh) the request in the db; could be empty if no
  -- email is associated; if a request already exists, will try to
  -- assign it the new pw, and extend its expiration.
  requestedOrConflict <- DB.requestPasswordReset requestingEmail hashedPw
  requested <- case requestedOrConflict of
    -- in case of `requested` being nothing (due to conflict,)
    -- attempt to "refresh." Can also return Nothing if there
    -- was nothing there (vs. a conflict) to begin with
    Nothing -> DB.refreshPasswordReset requestingEmail hashedPw
    Just done -> pure $ Just done
  case requested of
    Nothing -> do
      log $ Info "Invalid email"
      jetzt <- currentTimeUTC
      let fakeExpiration = nextDay jetzt
      -- 3a. return a fake expiration: we don't actually want it to be known
      -- that the email doesn't exist.
      pure . Right . Just $ fakeExpiration
    Just actualExpiration -> do
      --log $ Info $ "Requested password for " <> requestingEmail <> ": " <> generatedPassword
      -- 3b. send an email with the temp password: this is the last we see of it.
      emailSent <- sendEmailWithTemplate $ passwordResetEmail requestingEmail generatedPassword
      case emailSent of
        Left err -> do
          -- note that we only log the error, don't want to accidentally leak implementation
          -- details.
          log $ Error $ "Error sending email: " <> err
          -- this is the only error we're not obsfuscating, as a legitimate user has a right
          -- to know the email won't be arriving! Retrying should use the "refresh"
          -- action, since the db entry _was_ created.
          pure . Left $ "Server error, please try again later."
        Right _sent -> pure . Right . Just $ actualExpiration

nextDay :: UTCTime -> UTCTime
nextDay (UTCTime day time) = UTCTime (addDays 1 day) time

doResetPassword :: API.AppM sig m => Text -> Password -> Password -> m (Either Text DB.UserID)
doResetPassword email resetToken newPassword = do
  now <- currentTimeUTC
  resetRequest <- select $ DB.resetRequestByEmail email now
  case headMaybe resetRequest of
    -- purposeful false positive: don't want to tell an attacker
    -- that there actually never was a token for this email/time.
    Nothing -> pure . Left $ "Invalid token"
    Just (_uid, pwHash, expiresAt) -> do
      if expiresAt <= now then
        pure . Left $ "Expired token."
      else
        case checkPassword resetToken (PasswordHash pwHash) of
          PasswordCheckFail -> pure . Left $ "Invalid token."
          PasswordCheckSuccess -> do
            case validatePW newPassword of
              InvalidPassword reasons -> pure . Left $ catReasons reasons
              ValidPassword -> do
                hashedPassword <- hashPasswordArgon2 newPassword
                updated <- DB.resetPassword email hashedPassword
                case updated of
                  Nothing -> pure . Left $ "Invalid or expired token."
                  Just uid -> pure . Right $ uid

-- | Given reasons for the password being invalid, concatenate in one big
-- explanation.
catReasons :: [InvalidReason] -> Text
catReasons rs =
  T.intercalate "\n" $ map explain rs
  where
    explain =
      \case
        PasswordTooShort min' given ->
          "Password too short: must be at least " <> show min' <>
          " characters long, was only " <> show given <> "."
        PasswordTooLong max' given ->
          "Password too long: must be at most " <> show max' <>
          " characters long, was only " <> show given <> "."
        NotEnoughReqChars category min' given ->
          "Not enough " <> show category <> " characters: expected at least "
          <> show min' <> " but only " <> show given <> " given."
        InvalidCharacters invalidChars ->
          "Password must only contain valid typeable characters, " <> invalidChars
          <> " is not valid."
