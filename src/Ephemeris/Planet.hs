module Ephemeris.Planet where

import Ephemeris.Types
  ( Planet (..),
  )
import Import (Semigroup ((<>)))

defaultPlanets :: [Planet]
defaultPlanets = [Sun .. Pluto] <> [MeanNode, MeanApog, Chiron]
