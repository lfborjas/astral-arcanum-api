import Test.Hspec

import qualified Integration.EphemerisApiSpec as EphemerisApiSpec

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "EphemerisApi" EphemerisApiSpec.spec
