{-# LANGUAGE NamedFieldPuns #-}
module Api.Resolvers.Common where

import Import
import Api.Error
import qualified Api.Schema as API
import qualified Models.UserAccount as DB

currentUser :: (Has (Reader API.RequestContext) sig m) => m (Maybe DB.UserID)
currentUser = do
  API.RequestContext{API.currentUserID} <- ask
  pure currentUserID

requireAuth :: (Has (Throw ServerError) sig m, Has (Reader API.RequestContext) sig m) => m DB.UserID
requireAuth = do
  --API.RequestContext{API.currentUserID} <- ask
  currentUserID <- currentUser
  case currentUserID of
    Nothing -> throwError $ serverError "Not signed in."
    Just uid -> pure uid
