{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Ephemeris.Types
  ( -- re-exports from SwissEphemeris
    Angles (..),
    Planet
      ( Sun,
        Moon,
        Mercury,
        Venus,
        Mars,
        Jupiter,
        Saturn,
        Uranus,
        Neptune,
        Pluto,
        MeanNode,
        TrueNode,
        MeanApog,
        OscuApog,
        Chiron
      ),
    EclipticPosition (..),
    HouseSystem (..),
    HouseCusp,
    ZodiacSignName (..),
    SplitDegreesOption (..),
    JulianDayTT, JulianDayUT1,
    GeographicPosition (..),
    EquatorialPosition (..),
    ObliquityInformation (..),
    CuspsCalculation (..),
    LongitudeComponents (..),
    -- own types
    HasLongitude (..),
    HasLabel (..),
    HasSpeed(..),
    Element (..),
    ZodiacSign (..),
    HouseNumber (..),
    House (..),
    AspectName (..),
    AspectTemperament (..),
    AspectType (..),
    AspectPhase (..),
    Aspect (..),
    HoroscopeAspect (..),
    Latitude (..),
    Longitude (..),
    PlanetPosition (..),
    HoroscopeData (..),
    MomentData(..),
    Transit (..),
    TransitData (..),
    EclipticAngle (..),
    AspectAngle (..),
    -- type aliases
    AxisAspect,
    PlanetaryAspect,
    PlanetaryTransit,
    AxisTransit,
    TransitAspect,
    -- constraings
    Ecliptic
  )
where

import Import
import SwissEphemeris
  ( Angles (..),
    CuspsCalculation (..),
    EclipticPosition (..),
    EquatorialPosition (..),
    GeographicPosition (..),
    HouseCusp,
    HouseSystem (..),
    LongitudeComponents (..),
    ObliquityInformation (..),
    Planet (..),
    SplitDegreesOption (..),
    ZodiacSignName (..),
  )
import SwissEphemeris.Time

class Eq a => HasLongitude a where
  getLongitude :: a -> Longitude
  getLongitudeRaw :: a -> Double
  getLongitudeRaw = un . getLongitude
  addLongitude :: a -> Longitude -> a

class Eq a => HasSpeed a where
  getSpeed :: a -> Double

class Show a => HasLabel a where
  label :: a -> String
  label = show

instance HasLabel Planet where
  label MeanApog = "Lilith"
  label MeanNode = "Mean Node"
  label p = show p

instance HasLabel ZodiacSignName

data Element
  = Earth
  | Air
  | Fire
  | Water
  deriving stock (Eq, Show, Enum, Bounded)
  deriving anyclass (HasLabel)

data ZodiacSign = ZodiacSign
  { name :: ZodiacSignName,
    zodiacLongitude :: Longitude,
    zodiacElement :: Element
  }
  deriving stock (Eq, Show)

data HouseNumber
  = I
  | II
  | III
  | IV
  | V
  | VI
  | VII
  | VIII
  | IX
  | X
  | XI
  | XII
  deriving stock (Eq, Show, Ord, Enum, Bounded, Generic)

instance HasLabel HouseNumber where
  label I = "Asc"
  label IV = "IC"
  label VII = "Desc"
  label X = "MC"
  label h = show h

data House = House
  { houseNumber :: HouseNumber,
    houseCusp :: Longitude,
    houseDeclination :: Double
  }
  deriving stock (Eq, Show)

instance HasLabel House where
  label = label . houseNumber 

instance HasLongitude House where
  getLongitude = houseCusp
  addLongitude h l = h {houseCusp = houseCusp h + l}

instance HasSpeed House where
  -- FIXME: in fact, house cusps _do_ have a speed,
  -- and swiss ephemeris can calculate it in the "extended"
  -- method
  getSpeed = const 0.0

-- see: https://en.wikipedia.org/wiki/Astrological_aspect

data AspectName
  = Conjunction
  | Sextile
  | Square
  | Trine
  | Opposition
  | Quincunx
  | SemiSextile
  | Quintile
  | BiQuintile
  | Septile
  | SemiSquare
  | Novile
  | Sesquisquare -- Trioctile
  deriving stock (Eq, Show, Ord, Enum, Bounded, Generic)
  deriving anyclass (HasLabel)

data AspectTemperament
  = Analytical -- "Disharmonious"
  | Synthetic -- "Harmonious"
  | Neutral
  deriving stock (Eq, Show, Ord, Enum, Bounded, Generic)
  deriving anyclass (HasLabel)

data AspectType
  = Major
  | Minor
  deriving stock (Eq, Show, Ord, Enum, Generic)

-- | Is the aspecting body approaching, or leaving, exactitude?
-- NOTE: we assume that the aspected body is static, which is a correct
-- assumption for transits, in which the aspected natal bodies are fixed,
-- but it's not necessarily correct for natal charts, in which both
-- bodies were in motion.
-- More on these:
-- https://www.astro.com/astrowiki/en/Applying_Aspect
-- https://www.astro.com/astrowiki/en/Separating_Aspect
data AspectPhase
  = Applying
  | Separating
  | Exact
  deriving stock (Eq, Show, Ord, Enum, Generic)

instance HasLabel AspectPhase where
  label Applying = "a"
  label Separating = "s"
  label Exact = ""

newtype EclipticAngle = EclipticAngle {unEclipticAngle :: Double}
  deriving newtype (Eq, Show, Num)

data AspectAngle = AspectAngle
  { aspectingPosition :: EclipticAngle,
    aspectedPosition :: EclipticAngle,
    aspectAngleApparentPhase :: AspectPhase,
    aspectAngleOrb :: Double
  }
  deriving stock (Eq, Show)

data Aspect = Aspect
  { aspectName :: AspectName,
    maxOrb :: Double,
    angle :: Double,
    temperament :: AspectTemperament,
    aspectType :: AspectType
  }
  deriving stock (Eq, Show)

data HoroscopeAspect a b = HoroscopeAspect
  { aspect :: Aspect,
    bodies :: (a, b),
    aspectAngle :: AspectAngle
  }
  deriving stock (Eq, Show)

type PlanetaryAspect = HoroscopeAspect PlanetPosition PlanetPosition

type AxisAspect = HoroscopeAspect PlanetPosition House

type TransitAspect a = HoroscopeAspect PlanetPosition a

newtype Latitude = Latitude {unLatitude :: Double}
  deriving newtype (Eq, Show, Num, Ord)

newtype Longitude = Longitude {unLongitude :: Double}
  deriving newtype (Eq, Show, Num, Ord)

instance HasLongitude Longitude where
  getLongitude = id
  addLongitude = (+)

instance HasSpeed Longitude where
  getSpeed = const 0.0

data PlanetPosition = PlanetPosition
  { planetName :: Planet,
    planetLat :: Latitude,
    planetLng :: Longitude,
    planetLngSpeed :: Double,
    planetDeclination :: Double,
    planetHouse :: Maybe House
  }
  deriving stock (Eq, Show)

instance HasLongitude PlanetPosition where
  getLongitude = planetLng
  addLongitude p l = p {planetLng = planetLng p + l}

instance HasSpeed PlanetPosition where
  getSpeed = planetLngSpeed

instance HasLabel PlanetPosition where
  label = label . planetName

data HoroscopeData = HoroscopeData
  { horoscopePlanetPositions :: [PlanetPosition],
    horoscopeAngles :: Angles,
    horoscopeHouses :: [House],
    horoscopeSystem :: HouseSystem,
    -- TODO: all of the below could be derived ad-hoc.
    horoscopePlanetaryAspects :: [PlanetaryAspect],
    horoscopeAxisAspects :: [AxisAspect],
    horoscopeUniversalTime :: UTCTime,
    horoscopeJulianTime :: JulianDayUT1
    -- TODO: delta time?
  }
  deriving (Eq, Show)
  
data MomentData = MomentData
  { momentPlanetPositions :: [PlanetPosition],
    momentPlanetaryAspects :: [PlanetaryAspect],
    momentUniversalTime :: UTCTime,
    momentJulianTime :: JulianDayUT1
  }
  deriving (Eq, Show)
data TransitData = TransitData
  { natalPlanetPositions :: [PlanetPosition],
    natalAngles :: Angles,
    natalHouses :: [House],
    natalHouseSystem :: HouseSystem,
    transitingPlanetPositions :: [PlanetPosition],
    transitingHouses :: [House],
    transitingAngles :: Angles,
    transitingHouseSystem :: HouseSystem,
    planetaryTransits :: [(PlanetaryAspect, PlanetaryTransit)],
    angleTransits :: [(AxisAspect, AxisTransit)]
  }
  deriving (Eq, Show)

data Transit a = Transit
  { transiting :: PlanetPosition,
    transited :: a,
    transitStarts :: Maybe UTCTime,
    transitEnds :: Maybe UTCTime,
    immediateTriggers :: [UTCTime]
  }
  deriving stock (Eq, Show)

type PlanetaryTransit = Transit PlanetPosition

type AxisTransit = Transit House

type Ecliptic a = (HasSpeed a, HasLongitude a)
