{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE RankNTypes #-}

-- needed due to the way Unions are template-haskelled
{-# OPTIONS_GHC -Wno-partial-fields #-}
module Api.Schema where
import Import hiding (Float)
import Data.Morpheus.Document (importGQLDocument)
import Data.Morpheus.Types
import Effects
import Config
import Control.Carrier.Error.Either
import Api.Error
import Control.Carrier.Lift
import Web.Scotty
import Models.UserAccount (UserID)

-- | if @Float@ is in scope, @importGQLDocument@ will _insist_ on using it,
-- instead of @Double@ for its auto-derived schema. So we... hide it.
type Float = Double
$(importGQLDocument "api/schema.graphql")

type AppM sig m =
  ( Has (Log LogMessage) sig m,
    Has (Throw ServerError) sig m,
    Has EphemerisData sig m,
    Has TimeZoneData sig m,
    Has Database sig m,
    Has Crypto sig m,
    Has EmailRelay sig m,
    Has (Reader RequestContext) sig m
  )
  
type AppImpl =
  ErrorC ServerError
  (TimeZoneDataIOC
  (EphemerisDataIOC
  (DatabaseIOC 
  (CryptoIOC
  (EmailRelayIOC
  (ReaderC RequestContext
  (ReinterpretLogC LogMessage Text
  (LogStdoutC
  (LiftC ActionM)))))))))

-- convenience types (that we're not using)
-- | Resolve value
type Value o m a = ResolverO o () m a

-- | Resolve (f value)
type Composed o m f a = ComposedResolver o () m f a

-- | Data that all resolvers can have access to.
newtype RequestContext = RequestContext { currentUserID :: Maybe UserID }
