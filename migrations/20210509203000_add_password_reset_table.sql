CREATE EXTENSION IF NOT EXISTS btree_gist;

CREATE TABLE IF NOT EXISTS password_reset (
  user_account_id bigint not null references user_account on delete cascade,
  temp_password_hash text not null,
  expires_at timestamp with time zone default CURRENT_TIMESTAMP + INTERVAL '24 hours' not null,
  created_at timestamp with time zone default now() not null,
  updated_at timestamp with time zone default now() not null
);


drop trigger if exists password_reset_insert on password_reset;
  create trigger password_reset_insert before insert on password_reset for each row execute procedure create_timestamps();
drop trigger if exists password_reset_update on password_reset;
  create trigger password_reset_update before update on password_reset for each row execute procedure update_timestamps();

-- thanks, rimjobsteve: https://schinckel.net/tags/exclusion-constraint/  
ALTER TABLE password_reset
ADD CONSTRAINT prevent_overlapping_requests
EXCLUDE USING gist (
  user_account_id WITH =,
  TSTZRANGE(created_at, expires_at) WITH &&
)
