module Api.Main where
import Import
import Config ( defaultConfig, AppConfig (appDatabaseUrl, appJwtSecret) )
import System.Envy (decodeWithDefaults)
import Database.Migrations
import Options.Applicative
    ( Parser,
      fullDesc,
      help,
      info,
      long,
      progDesc,
      switch,
      execParser,
      helper,
      ParserInfo )
import qualified Api.Server as GraphQL

data Opts = Opts {optMigrate :: !Bool}

-- | Run server (or send the `migrate` flag to run migrations and exit)
main :: IO ()
main = do
  appConfig <- decodeWithDefaults defaultConfig
  opts <- execParser optsParser
  if optMigrate opts then do
    putStrLn "Migrations!"
    didMigrate <- runMigrations "migrations" (appDatabaseUrl appConfig)
    case didMigrate of
      Left e -> putStrLn $ "Error migrating: " <> e
      Right s -> putStrLn s
  else do
    --start appConfig
    putStrLn $ "Running with config" <> show appConfig{appJwtSecret="REDACTED"}
    GraphQL.runServer appConfig
  where
    -- see: https://www.fpcomplete.com/haskell/library/optparse-applicative/
    optsParser :: ParserInfo Opts
    optsParser =
      info
        (helper <*> mainOptions)
        (fullDesc <> progDesc "Run server, or migrate")
    mainOptions :: Parser Opts
    mainOptions =
      Opts <$> switch (long "migrate" <> help "Init migrations table idempotently, run missing migrations")
