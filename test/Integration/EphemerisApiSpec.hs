{-# LANGUAGE OverloadedStrings #-}
module Integration.EphemerisApiSpec (spec) where

-- import Import hiding (get)
-- import Carriers
-- import Test.Hspec (describe, Spec, it, shouldBe)
-- import Ephemeris.Types
-- import EphemerisApi.Types
-- import Effects (reinterpretLog)
-- import Config (renderLogMessage)
-- import Effects.Log (runLogStdout)
-- import EphemerisApi.Handlers (service)
-- import Test.Hspec.Wai

-- fixedEphemeris :: FixedEphemeris
-- fixedEphemeris =
--   FixedEphemeris planetPositions cusps obliquity
--   where
--     planetPositions =
--       [
--         (Sun, EclipticPosition {lng = 286.0, lat = -0.0001, distance = 0, lngSpeed = 1.01, latSpeed = 0, distSpeed =0 }),
--         (Saturn, EclipticPosition {lng = 276.32, lat = -0.0001, distance = 0, lngSpeed = 0.11, latSpeed = 0, distSpeed =0 }),
--         (Neptune, EclipticPosition {lng = 280.15, lat = -0.0001, distance = 0, lngSpeed = 0.03, latSpeed = 0, distSpeed =0 })
--       ]
--     cusps =
--         CuspsCalculation
--           [ 112.20189657163523,
--             138.4658382335878,
--             167.69682489058204,
--             199.79861981778183,
--             232.2797046698429,
--             263.0249102802477,
--             292.20189657163525,
--             318.46583823358776,
--             347.69682489058204,
--             19.798619817781823,
--             52.27970466984291,
--             83.02491028024768
--           ]
--           Angles
--             { ascendant = 112.20189657163523,
--               mc = 19.798619817781823,
--               armc = 18.277351820745423,
--               vertex = 216.1872418365295,
--               equatorialAscendant = 106.85773516967282,
--               coAscendantKoch = 101.19442735316477,
--               coAscendantMunkasey = 153.1221838791593,
--               polarAscendant = 281.19442735316477
--             }
--           Placidus
--     obliquity =
--       ObliquityInformation {eclipticObliquity = 23.44288555112768, eclipticMeanObliquity = 23.44070869609064, nutationLongitude = 1.9483531068634399e-3, nutationObliquity = 2.1768550370416455e-3}

-- fixedTimeZone :: FixedTimeZone
-- fixedTimeZone = FixedTimeZone "EST"

-- testApplication :: Application
-- testApplication =
--   serve proxyService $
--     hoistServer
--       proxyService
--       transform
--       service
--   where
--     transform handler = do
--       res <- runEffects
--       either throwError pure res
--       where
--         runEffects =
--           handler
--           & runTimeZoneTestWith fixedTimeZone
--           & runEphemerisTestWith fixedEphemeris 
--           & reinterpretLog renderLogMessage
--           & runLogStdout
--           & runError
--           & runM
          
-- spec :: Spec
-- spec = do
--   with (pure testApplication) $ do
--     -- little canary test
--     describe "GET /api-docs.html" $ do
--       it "produces birth data" $ do
--         get "/api-docs.html" `shouldRespondWith` 200

import Test.Hspec ( Spec, describe, it, shouldBe )
spec :: Spec
spec = do
  describe "FIXME" $ do
    it "needs tests" $ do
      42 `shouldBe` 42
