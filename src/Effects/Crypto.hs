{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Effects.Crypto where

import Control.Algebra
import Data.Password.Argon2
import Import
import Web.JWT hiding (alg, decode)
import Domain.Auth ( CustomClaims, JWS(..), mkJWTClaims )
import Control.Monad.Random.Class
import Data.List ( sum, (\\), delete )
-- Algebra

data Crypto (m :: Type -> Type) k where
  -- | Given a password, return a hashed version
  HashPasswordArgon2 :: Password -> Crypto m (PasswordHash Argon2)
  -- | Given a map of custom claims, generate a signed JWS
  -- with an iat claim.
  SignJWT :: CustomClaims -> Crypto m JWS
  GetRandomPassword :: Crypto m Text

hashPasswordArgon2 :: Has Crypto sig m => Password -> m (PasswordHash Argon2)
hashPasswordArgon2 = send . HashPasswordArgon2

signJWT :: Has Crypto sig m => CustomClaims -> m JWS
signJWT = send . SignJWT

getRandomPassword  :: Has Crypto sig m => m Text
getRandomPassword = send GetRandomPassword

-- Carriers
newtype CryptoIOC m a = CryptoIOC {runCryptoIO :: ReaderC Text m a}
  deriving (Applicative, Functor, Monad, MonadIO, MonadFail)

runCryptoWithSecret :: Text -> CryptoIOC m hs -> m hs
runCryptoWithSecret secret = runReader secret . runCryptoIO

instance
  (MonadIO m, Algebra sig m) =>
  Algebra (Crypto :+: sig) (CryptoIOC m)
  where
  alg hdl sig ctx = CryptoIOC $ case sig of
    L (HashPasswordArgon2 pw) -> do
      (<$ ctx) <$> liftIO (hashPassword pw)
    L (SignJWT claims') -> do
      secret <- ask
      currentTime <- liftIO getCurrentTime
      let cs = mkJWTClaims currentTime claims'
          signer = hmacSecret secret
          encoded = pure $ JWS $ encodeSigned signer mempty cs
      (<$ ctx) <$> encoded
    L GetRandomPassword -> do
      pw <- liftIO generateRandomPassword
      pure ((<$ ctx) pw)
    R other -> alg (runCryptoIO . hdl) (R other) ctx

---
--- RANDOM STUFF, literally
---

randomPassword :: MonadRandom m => [String] -> Int -> m String
randomPassword charSets n = do
  parts <- getPartition n
  chars <- zipWithM replicateM parts (uniform <$> charSets)
  shuffle (concat chars)
  where
    getPartition n' = adjust <$> replicateM (k-1) (getRandomR (1, n' `div` k))
    k = length charSets
    adjust p = (n - Data.List.sum p) : p

shuffle :: (Eq a, MonadRandom m) => [a] -> m [a]
shuffle [] = pure []
shuffle lst = do
  x <- uniform lst
  xs <- shuffle $ delete x lst
  return (x : xs)

-- | Default password generation: 6 characters, readable
-- charset. Generate as Text instead of the underlying String.
generateRandomPassword :: MonadRandom m => m Text
generateRandomPassword = do
  let l = 8
      s = zipWith (\\) charSets visuallySimilar
  toText <$> randomPassword s l
  where
    charSets =
      [ ['a' .. 'z']
      , ['A' .. 'Z']
      , ['0' .. '9']
      ]
    visuallySimilar = ["l", "IOSZ", "012"]
