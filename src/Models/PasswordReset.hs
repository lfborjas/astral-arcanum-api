{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE QuasiQuotes #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Models.PasswordReset where
import Data.Profunctor.Product.TH
    ( makeAdaptorAndInstanceInferrable )
import Models.Common

import Models.UserAccount

import Opaleye

import Database.PostgreSQL.Simple.Types (Only (..))
import Effects.Database
import Import
import Database.PostgreSQL.Simple.SqlQQ
import Data.Password.Types (PasswordHash(PasswordHash))
import Data.CaseInsensitive (mk)


data PasswordReset' puid ptph eat =
  PasswordReset
    {
      userAccountId :: puid
    , tempPasswordHash :: ptph
    , expiresAt :: eat
    }

$(makeAdaptorAndInstanceInferrable "pPasswordReset" ''PasswordReset')

type PasswordReset =
  Entity
   (PasswordReset'
      UserID
      Argon2PasswordHash
      UTCTime
   )

type PasswordResetWrite =
  EntityWriteField
    (PasswordReset'
      UserIDWrite
      (Field SqlText)
      (Field SqlTimestamptz)
    )

type PasswordResetField =
  EntityField
    (PasswordReset'
      UserIDField
      (Field SqlText)
      (Field SqlTimestamptz )
    )

passwordResetTable :: Table PasswordResetWrite PasswordResetField
passwordResetTable =
  table "password_reset" . pEntity . withTimestampFields $
    pPasswordReset
      PasswordReset
        { userAccountId = pUserID $ UserID $ tableField "user_account_id"
        , tempPasswordHash = tableField "temp_password_hash"
        , expiresAt = tableField "expires_at"
        }

-- | Find the current reset request for the given email, if any.
resetRequestByEmail :: Text -> UTCTime -> Select (UserIDField, Field SqlText, Field SqlTimestamptz)
resetRequestByEmail email now = do
  request <- selectTable passwordResetTable
  user <- selectTable userAccountTable
  let requestInfo = record request
      userInfo = record user
      createdAt = recordCreatedAt request
      currentTime = sqlUTCTime now
  -- lil' example of a join:
  -- inspired by: https://gitlab.com/williamyaoh/haskell-orm-comparison/-/blob/master/opaleye-impl/src/Lib.hs#L276-285
  where_ $ userEmail userInfo .=== (toFields . mk $ email)
  where_ $ userID userInfo .=== userAccountId requestInfo
  where_ $ createdAt .<= currentTime .&& expiresAt requestInfo .> currentTime
  pure (userAccountId requestInfo, tempPasswordHash requestInfo, expiresAt requestInfo)

---
--- RAW QUERIES
---

-- | Given an email and a password hash, attempt to
-- assign a password reset entry. Can fail if there's already
-- one for the current time period, or if the user doesn't exist.
-- Returns the expiration time for the token.
requestPasswordReset :: Has Database sig m => Text -> Argon2PasswordHash -> m (Maybe UTCTime)
requestPasswordReset email (PasswordHash tempHash) = do
  -- NOTE(luis) can't really use ON CONFLICT DO UPDATE here since
  -- exclusion constraints are not supported:
  -- https://www.postgresql.org/docs/current/sql-insert.html
  -- https://stackoverflow.com/a/55920752
  r <- query [sql|
    INSERT INTO password_reset (user_account_id, temp_password_hash)
    SELECT id, ? FROM user_account where email = ?
    ON CONFLICT DO NOTHING
    RETURNING expires_at
  |] (tempHash, email)
  pure $ listToMaybe $ map fromOnly r

-- | Given an email and temp password, attempt to set a new temp password/expiration
-- IFF a password reset request already exists for the user _and_ it hasn't expired.
refreshPasswordReset :: Has Database sig m => Text -> Argon2PasswordHash -> m (Maybe UTCTime)
refreshPasswordReset email (PasswordHash tempHash) = do
  r <- query [sql|
    UPDATE password_reset
    SET expires_at = DEFAULT, temp_password_hash = ?
    FROM user_account
    WHERE email = ?
    AND user_account.id = password_reset.user_account_id
    AND tstzrange(password_reset.created_at, password_reset.expires_at) @> current_timestamp
    RETURNING expires_at;
  |] (tempHash, email)
  pure $ listToMaybe $ map fromOnly r

-- | Given an email and a temp password, set the corresponding user's
-- password IFF there's a valid token. Deletes the password_reset entry.
-- Returns the ID of the updated user (to use in authentication)
resetPassword :: Has Database sig m => Text -> Argon2PasswordHash -> m (Maybe UserID)
resetPassword email (PasswordHash newHash) = do
  r <- query [sql|
    WITH deleted AS (
      DELETE FROM password_reset
      USING user_account
      WHERE user_account.email = ? 
      AND   user_account.id = password_reset.user_account_id
      AND tstzrange(password_reset.created_at, password_reset.expires_at) @> current_timestamp 
      RETURNING user_account_id
    )
    UPDATE user_account
    SET password_hash = ?
    FROM deleted
    WHERE id = deleted.user_account_id
    RETURNING user_account.id
  |] (email, newHash)
  pure $ toUserID $ listToMaybe $ map fromOnly r
  where
    toUserID :: Maybe Int64 -> Maybe UserID
    toUserID = fmap UserID

-- NOTES
{- to run queries manually:
-- cabal repl
-- import qualified Database.PostgreSQL.Simple as PG
λ> let (DatabaseUrl url) = defaultConfig & appDatabaseUrl
λ> conn <- PG.connectPostgreSQL $ encodeUtf8 url
λ> runDatabaseWithConnection conn $ Database.Queries.cityCount 
196591
-}
