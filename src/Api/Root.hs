{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE NamedFieldPuns #-}
module Api.Root where
import Api.Schema (Query(..), Mutation(..), AppM)
import Data.Morpheus.Types
import Api.Resolvers.Horoscope
import Api.Resolvers.User

rootResolver :: AppM sig m => RootResolver m () Query Mutation Undefined
rootResolver =
  RootResolver
    {
      queryResolver = 
        Query
          {
            horoscope = resolveHoroscope
          , moment = resolveMoment
          , primarySavedChart = resolvePrimarySavedChart 
          , savedCharts = resolveOwnSavedCharts
          , savedChart = resolveChart
          , currentUser = resolveCurrentUser 
          },
      mutationResolver = 
        Mutation
          {
            signIn = resolveSignIn
          , signUp = resolveSignUp
          , updateProfile = resolveUpdateProfile
          , updateLogin = resolveUpdateCredentials
          , requestPasswordReset = resolveRequestPasswordReset
          , resetPassword = resolveResetPassword
          , saveChart = resolveSaveChart
          , updateChart = resolveUpdateChart
          , deleteChart = resolveDeleteChart
          },
      subscriptionResolver = Data.Morpheus.Types.Undefined
    }
