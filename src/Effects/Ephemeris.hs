{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Effects.Ephemeris where

import Control.Algebra
import Ephemeris.Types
import Import
import SwissEphemeris
import SwissEphemeris.Time ()

data EphemerisData (m :: Type -> Type) k where
  GetPlanetPosition :: Planet -> JulianDayUT1 -> EphemerisData m (Either String EclipticPosition)
  GetHouseCusps :: HouseSystem -> JulianDayUT1 -> GeographicPosition -> EphemerisData m CuspsCalculation
  GetObliquity :: JulianDayUT1 -> EphemerisData m (Either String ObliquityInformation)
  FromJulianUT1 :: JulianDayUT1 -> EphemerisData m UTCTime
  ToJulianUT1 :: UTCTime -> EphemerisData m (Either String JulianDayUT1)
  --FromJulian   :: UTCTime -> EphemerisData JulianDay ts

getPlanetPosition :: Has EphemerisData sig m => Planet -> JulianDayUT1 -> m (Either String EclipticPosition)
getPlanetPosition p t = send $ GetPlanetPosition p t

getHouseCusps :: Has EphemerisData sig m => HouseSystem -> JulianDayUT1 -> GeographicPosition -> m CuspsCalculation
getHouseCusps hs t p = send $ GetHouseCusps hs t p

getObliquity :: Has EphemerisData sig m => JulianDayUT1 -> m (Either String ObliquityInformation)
getObliquity = send . GetObliquity

fromJulianUT1 :: Has EphemerisData sig m => JulianDayUT1 -> m UTCTime
fromJulianUT1 = send . FromJulianUT1

toJulianUT1 :: Has EphemerisData sig m => UTCTime -> m (Either String JulianDayUT1)
toJulianUT1 = send . ToJulianUT1

newtype EphemerisDataIOC m a = EphemerisDataIOC {runEphemerisDataIO :: m a}
  deriving (Applicative, Functor, Monad, MonadIO, MonadFail)

instance (MonadIO m, Algebra sig m) => Algebra (EphemerisData :+: sig) (EphemerisDataIOC m) where
  alg hdl sig ctx = case sig of
    L (GetPlanetPosition p t) -> (<$ ctx) <$> liftIO (calculateEclipticPosition t p)
    L (GetHouseCusps hs t p) -> (<$ ctx) <$> liftIO (calculateCusps hs t p)
    L (GetObliquity t) -> (<$ ctx) <$> liftIO (calculateObliquity t)
    L (FromJulianUT1 t) -> (<$ ctx) <$> liftIO (fromJulianDay t)
    L (ToJulianUT1 t) -> (<$ ctx) <$> liftIO (toJulianDay t)
    R other -> EphemerisDataIOC (alg (runEphemerisDataIO . hdl) other ctx)

withEphemerisData :: FilePath -> IO a -> IO a
withEphemerisData = withEphemerides
