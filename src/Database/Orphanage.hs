{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Database.Orphanage where

import qualified Data.Profunctor.Product.Default as D
import qualified Data.Time as Time
import Opaleye (FromField)
import qualified Opaleye as T
import Opaleye.Internal.Inferrable

-- this is still a controversial instance upstream
-- https://github.com/tomjaguarpaw/haskell-opaleye/issues/495#issuecomment-854657433
instance utcTime ~ Time.UTCTime
  => D.Default (Inferrable FromField) T.SqlTimestamptz utcTime where
  def = Inferrable D.def
