let
  pkgs = import ./packages.nix {};
in
  { astral-arcanum = pkgs.haskellPackages.astral-arcanum; }
