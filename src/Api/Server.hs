{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE FlexibleContexts #-}
module Api.Server where

import Config
import Import
import qualified Network.Wai.Handler.Warp as Warp
import Network.Wai.Middleware.Cors
import Effects
import Network.Wai (Application)
import Web.Scotty
import Network.HTTP.Types (status200)
import Data.Morpheus (interpreter)
import qualified Data.ByteString.Lazy as LB
import Api.Error
import Api.Schema
import Api.Root
import qualified Data.Pool as P
import qualified Database.PostgreSQL.Simple as PG
import qualified Database.Pool as DB
import Data.Time.LocalTime.TimeZone.Detect (TimeZoneDatabase)
import Domain.Auth (extractUserID)

runServer :: AppConfig -> IO ()
runServer cfg =
  withEphemerisData (appEphemerisFolder cfg) $
    withTimeZoneData (appTimeZoneFile cfg) $ \tzdb -> do
        pool <- DB.initPool (appDatabaseUrl cfg)
        let appSecrets = AppSecrets {jwtSecret = appJwtSecret cfg, postmarkApiToken = appPostmarkApiToken cfg}
            env = AppContext {timeZoneDatabase = tzdb, databasePool = pool, secrets = appSecrets}
        app <- application env
        Warp.run (appPort cfg) (corsMiddleware app)
        where
          corsMiddleware = cors $ const $ Just corsPolicy
          -- bypass-tunnel-reminder if merely to sate localtunnel for dev.
          extraHeaders = ["Authorization", "Content-Type", "Bypass-Tunnel-Reminder"]
          corsPolicy =
            simpleCorsResourcePolicy {
              corsRequestHeaders = simpleHeaders <> extraHeaders,
              corsMethods = simpleMethods <> ["OPTIONS"]
            }
            
application :: AppContext -> IO Application
application AppContext{timeZoneDatabase, databasePool, secrets} = 
  scottyApp $ do
    post "/" $ do
      reqBody <- body
      -- NOTE(luis) put the headers in a Reader context,
      -- so individual actions can read them? Or have
      -- a specific effect for authentication?
      authHeader <- header "authorization"
      let perhapsUserID = extractUserID (jwtSecret secrets) authHeader 
          requestContext = RequestContext perhapsUserID
      res <- P.withResource databasePool $ \conn -> 
        runEffects conn timeZoneDatabase secrets requestContext $ api reqBody
      setHeader "Content-Type" "application/json; charset=utf-8"
      status status200
      case res of
        Right r -> do
          raw r
        Left e -> do
          raw $ renderErrorResponse e
      

api :: LB.ByteString -> AppImpl LB.ByteString 
api = interpreter rootResolver

runEffects :: PG.Connection
  -> TimeZoneDatabase
  -> AppSecrets
  -> RequestContext
  -> AppImpl a
  -> ActionM (Either ServerError a)
runEffects conn timeZoneDatabase secrets requestCtx =
  runError @ServerError
    >>> runTimeZoneDataWithDB timeZoneDatabase
    >>> runEphemerisDataIO
    >>> runDatabaseWithConnection conn
    >>> runCryptoWithSecret (jwtSecret secrets)
    >>> runEmailWithToken (postmarkApiToken secrets)
    >>> runReader requestCtx
    >>> reinterpretLog renderLogMessage
    >>> runLogStdout
    >>> runM
