{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE DeriveFunctor #-}
module Models.UserAccount where

import Data.CaseInsensitive (CI, mk)
import Data.Profunctor.Product.TH (makeAdaptorAndInstanceInferrable)
import Import
import Models.Common
import Opaleye
import Data.Password.Types (PasswordHash(..))
import Data.Profunctor.Product.Default (Default(..))
import Data.Password.Argon2 (Argon2)

-- inspired by:
-- http://hackage.haskell.org/package/password-instances-3.0.0.0/docs/src/Data.Password.Instances.html
instance DefaultFromField SqlText (PasswordHash a) where
  defaultFromField = PasswordHash <$> defaultFromField

instance Default ToFields (PasswordHash a) (Column SqlText) where
  def = toToFields (\(PasswordHash txt) -> sqlStrictText txt)

newtype UserID' a = UserID a
  deriving newtype (Eq, Show)
  deriving Functor

$(makeAdaptorAndInstanceInferrable "pUserID" ''UserID')

type UserIDField = UserID' (Field SqlInt8)
type UserIDWrite = UserID' (Maybe (Field SqlInt8))
type UserID = UserID' Int64

data UserAccount' uid uname uemail upw =
  UserAccount
    { userID :: uid
    , userName :: uname
    , userEmail :: uemail
    , userPasswordHash :: upw
    }

-- | 
type Argon2PasswordHash = PasswordHash Argon2
-- | Default UserAccount model is defined only for argon2
type UserAccount =
  Entity
    (UserAccount'
      UserID
      (Maybe Text)
      (CI Text)
      Argon2PasswordHash)

type UserAccountWrite =
  EntityWriteField
    (UserAccount'
      UserIDWrite
      (FieldNullable SqlText)
      (Field SqlCitext)
      (Field SqlText))

type UserAccountField =
  EntityField
    (UserAccount'
      UserIDField
      (FieldNullable SqlText)
      (Field SqlCitext)
      (Field SqlText))

$(makeAdaptorAndInstanceInferrable "pUserAccount" ''UserAccount')

userAccountTable ::
  Table UserAccountWrite UserAccountField
userAccountTable =
  table "user_account" . pEntity . withTimestampFields $
    pUserAccount
      UserAccount
        { userID = pUserID (UserID $ optionalTableField "id")
        , userName = tableField "name"
        , userEmail = requiredTableField "email"
        , userPasswordHash = requiredTableField "password_hash"
        }

---
--- QUERIES
---

newUser :: Maybe Text -> Text -> Argon2PasswordHash -> Insert [(UserID, Maybe Text, CI Text)]
newUser name email pwHash =
  Insert
    { iTable = userAccountTable
    , iRows = withTimestamp [row]
    , iReturning = rReturning (\Entity{record} -> (userID record, userName record, userEmail record))
    , iOnConflict = Just DoNothing
    }
  where
    row =
      UserAccount
        (UserID Nothing)
        (toFields name)
        (toFields . mk $ email)
        (toFields pwHash)

-- | Get a user by email _including their password hash_
userByEmailForLogin :: Text -> Select (UserIDField, FieldNullable SqlText, Field SqlCitext, Field SqlText)
userByEmailForLogin email = do
  Entity{record} <- selectTable userAccountTable
  where_ $ userEmail record .=== (toFields . mk $ email)
  pure (userID record , userName record , userEmail record, userPasswordHash record)

userByEmail :: Text -> Select (FieldNullable SqlText, Field SqlCitext, Field SqlTimestamptz)
userByEmail email = do
  Entity{record, recordCreatedAt} <- selectTable userAccountTable
  where_ $ userEmail record .=== (toFields . mk $ email)
  pure (userName record, userEmail record, recordCreatedAt)

userByID :: UserID -> Select (UserIDField, FieldNullable SqlText, Field SqlCitext)
userByID uid = do
  Entity{record} <- selectTable userAccountTable
  where_ $ userID record .=== toFields uid
  pure (userID record, userName record, userEmail record)

userCredsById :: UserID -> Select (UserIDField, Field SqlText)
userCredsById uid = do
  Entity{record} <- selectTable userAccountTable
  where_ $ userID record .=== toFields uid
  pure (userID record, userPasswordHash record)

updateUserProfileInfo :: UserID -> Maybe Text -> Update [(Maybe Text, CI Text, UTCTime)]
updateUserProfileInfo uid updatedName = do
  Update
    { uTable = userAccountTable
    , uUpdateWith = updateRecordWith updateProfileInfo
    , uWhere = (.===) (toFields uid) . userID . record
    , uReturning = rReturning (\Entity{record, recordUpdatedAt} -> (userName record, userEmail record, recordUpdatedAt))
    }
  where
    updateProfileInfo userEntity =
      userEntity{
        userID = Just <$> userID userEntity
      , userName = maybe (userName userEntity) (toFields . Just) updatedName
      }


updateUserCreds :: UserID -> Maybe Text -> Maybe Argon2PasswordHash -> Update [(Maybe Text, CI Text, UTCTime)]
updateUserCreds uid updatedEmail updatedPasswordHash = do
  Update
    { uTable = userAccountTable
    , uUpdateWith = updateRecordWith updateCredentials
    , uWhere = (.===) (toFields uid) . userID . record
    , uReturning = rReturning (\Entity{record, recordUpdatedAt} -> (userName record, userEmail record, recordUpdatedAt))
    }
  where
    updateCredentials userEntity =
      userEntity{
        userID = Just <$> userID userEntity
      , userEmail = maybe (userEmail userEntity) (toFields . mk) updatedEmail
      , userPasswordHash = maybe (userPasswordHash userEntity) toFields updatedPasswordHash
      }
