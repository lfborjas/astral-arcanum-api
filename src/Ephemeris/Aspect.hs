{-# LANGUAGE RecordWildCards #-}

module Ephemeris.Aspect where

import Ephemeris.Types
import Ephemeris.Utils (isRetrograde)
import Import

-- TODO: pull these up to be actual bindings like `conjunction`, `square`, etc.
majorAspects :: [Aspect]
majorAspects =
  [ Aspect {aspectType = Major, aspectName = Conjunction, angle = 0.0, maxOrb = 10.0, temperament = Synthetic},
    Aspect {aspectType = Major, aspectName = Sextile, angle = 60.0, maxOrb = 6.0, temperament = Synthetic},
    Aspect {aspectType = Major, aspectName = Square, angle = 90.0, maxOrb = 10.0, temperament = Analytical},
    Aspect {aspectType = Major, aspectName = Trine, angle = 120.0, maxOrb = 10.0, temperament = Synthetic},
    Aspect {aspectType = Major, aspectName = Opposition, angle = 180.0, maxOrb = 10.0, temperament = Analytical}
  ]

minorAspects :: [Aspect]
minorAspects =
  [ Aspect {aspectType = Minor, aspectName = SemiSquare, angle = 45.0, maxOrb = 3.0, temperament = Analytical},
    Aspect {aspectType = Minor, aspectName = Sesquisquare, angle = 135.0, maxOrb = 3.0, temperament = Analytical},
    Aspect {aspectType = Minor, aspectName = SemiSextile, angle = 30.0, maxOrb = 3.0, temperament = Neutral},
    Aspect {aspectType = Minor, aspectName = Quincunx, angle = 150.0, maxOrb = 3.0, temperament = Neutral},
    Aspect {aspectType = Minor, aspectName = Quintile, angle = 72.0, maxOrb = 2.0, temperament = Synthetic},
    Aspect {aspectType = Minor, aspectName = BiQuintile, angle = 144.0, maxOrb = 2.0, temperament = Synthetic}
  ]

defaultAspects :: [Aspect]
defaultAspects = majorAspects <> minorAspects

-- | Calculate aspects to use for transit insights.
-- Note that we use the default orbs.
-- However, to consider the aspect "active", we use a smaller orb, of 1 degree.
-- cf.: https://www.astro.com/astrowiki/en/Transit
aspectsForTransits :: [Aspect]
aspectsForTransits = defaultAspects -- map (\a -> a{maxOrb = 5.0}) majorAspects

aspects' :: (Ecliptic a, Ecliptic b) => [Aspect] -> [(a, b)] -> [HoroscopeAspect a b]
aspects' possibleAspects pairs =
  concatMap aspectsBetween pairs & catMaybes
  where
    aspectsBetween bodyPair = map (haveAspect bodyPair) possibleAspects
    haveAspect (a, b) asp =
      findAspectAngle asp a b <&> HoroscopeAspect asp (a, b)

aspects ::(Ecliptic a, Ecliptic b) => [(a,b)] -> [HoroscopeAspect a b]
aspects = aspects' defaultAspects

-- | calculate aspects between the same set of planets. Unlike `transitingAspects`, don't
-- keep aspects of a planet with itself.
planetaryAspects :: [PlanetPosition] -> [HoroscopeAspect PlanetPosition PlanetPosition]
planetaryAspects ps = 
  aspects pairs
  where
    -- unique pairs, as per:
    -- https://stackoverflow.com/questions/34044366/how-to-extract-all-unique-pairs-of-a-list-in-haskell/34045121
    -- sort by descending speed: faster planets should be considered to be aspecting slower ones.
    pairs = [(aspecting, aspected) | (aspecting:ys) <- tails bySpeed, aspected <- ys]
    bySpeed = sortBy (\p1 p2 -> compare (planetLngSpeed p2) (planetLngSpeed p1)) ps

celestialAspects :: [PlanetPosition] -> Angles -> [HoroscopeAspect PlanetPosition House]
celestialAspects ps as = 
  aspects pairs
  where 
    pairs = [(aspecting, aspected) | aspecting <- ps, aspected <- aspectableAngles as]

aspectableAngles :: Angles -> [House]
aspectableAngles Angles {..} = [House I (Longitude ascendant) 0, House X (Longitude mc) 0]

-- | Given a list of aspects, keep only major aspects.
-- useful as a helper when plotting/showing tables.
selectMajorAspects :: [HoroscopeAspect a b] -> [HoroscopeAspect a b]
selectMajorAspects = filter ((== Major) . aspectType . aspect)

-- | Select aspects with an orb of at most 1 degree. Useful for plotting.
selectExactAspects :: [HoroscopeAspect a b] -> [HoroscopeAspect a b]
selectExactAspects = filter ((<= 1) . orb)

-- | Whittle down a list of aspects to only applying aspects, and
-- recently active separating aspects.
selectSignificantAspects :: [TransitAspect a] -> [TransitAspect a]
selectSignificantAspects  =
  filter isActiveAspect
  where
    isActiveAspect a =
      not ((aspectPhase a == Separating) && (orb a <= 1))

-- TODO(luis): these find* functions are _so_ wasteful. We could clearly do it in one pass vs. traverse the whole
-- list for each planet. However, I always find myself updating this file at midnight when my neurons are
-- not ready for the magic.
findAspectBetweenPlanets :: [HoroscopeAspect PlanetPosition PlanetPosition] -> Planet -> Planet -> Maybe (HoroscopeAspect PlanetPosition PlanetPosition)
findAspectBetweenPlanets aspectList pa pb =
  aspectList
    & filter (\HoroscopeAspect {..} -> bimap planetName planetName bodies `elem` [(pa, pb), (pb, pa)])
    & headMaybe

findAspectWithPlanet :: [PlanetaryAspect] -> Planet -> Planet -> Maybe PlanetaryAspect
findAspectWithPlanet aspectList aspecting aspected =
  aspectList
    & filter (\HoroscopeAspect {..} -> bimap planetName planetName bodies == (aspecting, aspected))
    & headMaybe

findAspectWithAngle :: [HoroscopeAspect PlanetPosition House] -> Planet -> HouseNumber -> Maybe (HoroscopeAspect PlanetPosition House)
findAspectWithAngle aspectList pa hb =
  aspectList
    & filter (\HoroscopeAspect {..} -> bimap planetName houseNumber bodies == (pa, hb))
    & headMaybe

findAspectsByName :: [HoroscopeAspect a b] -> AspectName -> [HoroscopeAspect a b]
findAspectsByName aspectList name =
  aspectList
    & filter (\HoroscopeAspect {..} -> (aspect & aspectName) == name)

findAspectAngle :: (Ecliptic a, Ecliptic b) => Aspect -> a -> b -> Maybe AspectAngle
findAspectAngle aspect aspecting aspected =
  aspectAngle' aspect aspecting aspected
    <|> aspectAngle' aspect (aspecting `addLongitude` 360) aspected
    <|> aspectAngle' aspect aspecting (aspected `addLongitude` 360)


aspectAngle' :: (Ecliptic a, Ecliptic b) => Aspect -> a -> b -> Maybe AspectAngle
aspectAngle' Aspect {..} aspecting aspected =
  if inOrb
    then case (comparedAtSpeed, compare angleDiff angle) of
      (LT, GT) -> mkAngle Applying
      (LT, LT) -> mkAngle Separating
      (_, EQ) -> mkAngle Exact
      (EQ, _) -> mkAngle Exact
      (GT, GT) -> mkAngle Separating
      (GT, LT) -> mkAngle Applying
    else Nothing
  where
    comparedAtSpeed =
      if getSpeed aspecting >= getSpeed aspected then
        compare (getLongitude aspecting) (getLongitude aspected)
      else
        compare (getLongitude aspected) (getLongitude aspecting)
    mkAngle = Just . (\phase -> AspectAngle aspecting' aspected' phase orb')
    aspecting' = EclipticAngle $ getLongitudeRaw aspecting
    aspected' = EclipticAngle $ getLongitudeRaw aspected
    angleDiff = abs $ getLongitudeRaw aspecting - getLongitudeRaw aspected
    orb' = abs $ angle - angleDiff
    inOrb = orb' <= maxOrb

toLongitude :: EclipticAngle -> Longitude
toLongitude (EclipticAngle e)
  | e > 360 = Longitude . abs $ 360 - e
  | e == 360 = Longitude 0
  | e < 0 = Longitude . abs $ 360 + e
  | otherwise = Longitude e

exactAngle :: HoroscopeAspect a b -> Longitude
exactAngle aspect' =
  case aspectAngleApparentPhase angle' of
    Applying -> EclipticAngle (aspecting' + orb') & toLongitude
    Separating -> EclipticAngle (aspecting' - orb') & toLongitude
    Exact -> a & toLongitude
  where
    angle' = aspectAngle aspect'
    orb' = aspectAngleOrb angle'
    a@(EclipticAngle aspecting') = aspectingPosition angle'

currentAngle :: HoroscopeAspect a b -> EclipticAngle
currentAngle HoroscopeAspect {..} =
  abs $ (aspectAngle & aspectingPosition) - (aspectAngle & aspectedPosition)

orb :: HoroscopeAspect a b -> Double
orb = aspectAngleOrb . aspectAngle

aspectPhase :: TransitAspect a -> AspectPhase
aspectPhase asp =
  if aspectingIsRetrograde
    then flipPhase $ aspectAngleApparentPhase angle'
    else aspectAngleApparentPhase angle'
  where
    flipPhase Applying = Separating
    flipPhase Separating = Applying
    flipPhase Exact = Exact
    angle' = aspectAngle asp
    aspectingIsRetrograde = asp & bodies & fst & isRetrograde
