module Domain.Auth where

import Import
import Web.JWT
import Models.UserAccount
import Data.Map(lookup)
import qualified Data.Text.Lazy as LT
import Data.Aeson.Types
import Data.Time.Clock.POSIX (utcTimeToPOSIXSeconds)

-- | A signed JWT
newtype JWS = JWS Text
-- TODO: derive ToJSON?

-- | A map of custom claims to encode in JWTs
type CustomClaims = Map Text Value

-- | Given the raw value from the authorization header, extract the token
extractAuthToken :: Maybe LT.Text -> Maybe JWS
extractAuthToken headerVal =
  headerVal
  <&> LT.splitAt (LT.length "Bearer ")
  <&> snd 
  <&> toStrict
  <&> JWS

verifyTokenClaims :: Text -> JWS -> Maybe CustomClaims
verifyTokenClaims secret (JWS token) =
  verified >>= (claims >>> unregisteredClaims  >>> unClaimsMap >>> pure)
  where
    verified = decodeAndVerifySignature signer token 
    signer = hmacSecret secret

-- | Given a JWS secret and the current request's 'Authorization' header,
-- attempt to find the user id in the unregistered claims map; may
-- fail to extract or verify.
extractUserID :: Text -> Maybe LT.Text -> Maybe UserID
extractUserID secret headerVal = do
  tok <- extractAuthToken headerVal
  verifiedClaims <- verifyTokenClaims secret tok
  uid <- lookup "uid" verifiedClaims
  case fromJSON uid of
    Error _ -> Nothing
    Success id' -> Just $ UserID id'
    
mkUserClaims :: UserID -> CustomClaims
mkUserClaims (UserID uid) =
  fromList [("uid", toJSON uid)]

mkJWTClaims :: UTCTime -> CustomClaims ->JWTClaimsSet 
mkJWTClaims ut claims' = 
  mempty
    {
      iss = stringOrURI "astral-arcanum-api",
      aud = Left <$> stringOrURI "astral-arcanum-app",
      iat = numericDate $ utcTimeToPOSIXSeconds ut,
      unregisteredClaims = ClaimsMap claims'
    }
